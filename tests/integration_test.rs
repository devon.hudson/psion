use async_std::task;
use futures::StreamExt;
use futures::{channel::mpsc, FutureExt};
use futures::{
    future::{self, join_all},
    select, Future,
};
use futures_timer::Delay;
use psion::{
    generate_node_keypair, Libp2pRouter, OverlaySocket, PeerAddress, Router, RouterBuilder,
    RouterEvent, RouterExecutor,
};
use serial_test::serial;
use std::{pin::Pin, time::Duration};

#[allow(dead_code)]
fn enable_logging() {
    std::env::set_var("RUST_LOG", "debug");
    env_logger::init();
}

fn async_executor() -> impl RouterExecutor {
    |fut| {
        task::spawn(fut);
    }
}

fn null_executor() -> impl RouterExecutor {
    |_fut| {}
}

async fn join_or_timeout_and_panic<T>(
    futures: Vec<Pin<Box<dyn Future<Output = T> + '_>>>,
    timeout: Duration,
) -> Vec<T> {
    let process_futures = async { join_all(futures).await };
    let test_timeout = async { Delay::new(timeout).await };
    select! {
        future_results = process_futures.fuse() => future_results,
        _ = test_timeout.fuse() => panic!(),
    }
}

// NOTE : This is serial because local discovery between tests can happen and alter the behaviour.
#[test]
#[serial]
fn should_create_overlay_socket_from_libp2p_router() {
    let (priv_key, pub_key) = generate_node_keypair();
    let mut router = RouterBuilder::<Libp2pRouter>::new(priv_key, pub_key)
        .build(null_executor())
        .unwrap();

    let (tx_channel, rx_channel) = router.get_overlay_socket_channels().unwrap();
    let _socket = OverlaySocket::new(tx_channel, rx_channel);
}

// NOTE : This is serial because local discovery between tests can happen and alter the behaviour.
#[async_std::test]
#[serial]
async fn should_discover_and_connect_to_local_peer() {
    // Given: Two nodes are created with no known peers
    let (priv_key1, pub_key1) = generate_node_keypair();
    let (priv_key2, pub_key2) = generate_node_keypair();

    let (event_tx1, event_rx1) = mpsc::channel(32);
    let (event_tx2, event_rx2) = mpsc::channel(32);
    let _router1 = RouterBuilder::<Libp2pRouter>::new(priv_key1, pub_key1)
        .with_listen_addresses(vec![PeerAddress::SocketV4("127.0.0.1:0".parse().unwrap())])
        .with_static_peers(Vec::new())
        .with_event_channel(event_tx1)
        .build(async_executor())
        .unwrap();
    let _router2 = RouterBuilder::<Libp2pRouter>::new(priv_key2, pub_key2)
        .with_listen_addresses(vec![PeerAddress::SocketV4("127.0.0.1:0".parse().unwrap())])
        .with_static_peers(Vec::new())
        .with_event_channel(event_tx2)
        .build(async_executor())
        .unwrap();

    // When: Both nodes are run
    // Then: Both nodes discover each other and make a connection
    let mut filtered1 =
        event_rx1.filter(|ev| future::ready(matches!(ev, RouterEvent::ConnectionEstablished)));
    let mut filtered2 =
        event_rx2.filter(|ev| future::ready(matches!(ev, RouterEvent::ConnectionEstablished)));

    let _results = join_or_timeout_and_panic(
        vec![filtered1.next().boxed(), filtered2.next().boxed()],
        Duration::from_secs(3),
    )
    .await;
}

// NOTE : This is serial because local discovery between tests can happen and alter the behaviour.
#[async_std::test]
#[serial]
async fn should_pick_node_with_higher_key_as_root() {
    // Given: Two nodes are created
    let (priv_key1, pub_key1) = generate_node_keypair();
    let (priv_key2, pub_key2) = generate_node_keypair();

    let (event_tx1, event_rx1) = mpsc::channel(32);
    let (event_tx2, event_rx2) = mpsc::channel(32);
    let _router1 = RouterBuilder::<Libp2pRouter>::new(priv_key1, pub_key1)
        .with_listen_addresses(vec![PeerAddress::SocketV4("127.0.0.1:0".parse().unwrap())])
        .with_static_peers(Vec::new())
        .with_event_channel(event_tx1)
        .build(async_executor())
        .unwrap();
    let _router2 = RouterBuilder::<Libp2pRouter>::new(priv_key2, pub_key2)
        .with_listen_addresses(vec![PeerAddress::SocketV4("127.0.0.1:0".parse().unwrap())])
        .with_static_peers(Vec::new())
        .with_event_channel(event_tx2)
        .build(async_executor())
        .unwrap();

    // When: Both nodes are run
    let mut filtered1 =
        event_rx1.filter(|ev| future::ready(matches!(ev, RouterEvent::NewSpanningTreeRoot { .. })));
    let mut filtered2 =
        event_rx2.filter(|ev| future::ready(matches!(ev, RouterEvent::NewSpanningTreeRoot { .. })));

    // Both nodes will immediately advertise themselves as the root so ignore the first event.
    join_or_timeout_and_panic(
        vec![filtered1.next().boxed(), filtered2.next().boxed()],
        Duration::from_secs(5),
    )
    .await;

    // Then: the node with the higher key is chosen as the tree root
    let result;
    let higher_key;
    if pub_key1 > pub_key2 {
        result = join_or_timeout_and_panic(vec![filtered2.next().boxed()], Duration::from_secs(10))
            .await;
        higher_key = pub_key1;
    } else {
        result = join_or_timeout_and_panic(vec![filtered1.next().boxed()], Duration::from_secs(10))
            .await;
        higher_key = pub_key2;
    }

    let result = result[0].as_ref().unwrap();

    if let RouterEvent::NewSpanningTreeRoot {
        root_key,
        parent,
        coordinates,
    } = result
    {
        assert_eq!(*root_key, higher_key);
        assert_eq!(parent.unwrap(), higher_key);
        assert_eq!(*coordinates, vec![0]);
    } else {
        panic!();
    }
}
