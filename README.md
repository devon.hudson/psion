# Psion

Psion is a library containing peer-to-peer router experiments for the Matrix protocol.

## Example Router

To test out the router you can run the following:

```
cargo run --example psion-libp2p
```
