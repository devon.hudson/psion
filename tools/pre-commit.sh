#!/bin/bash
echo "Run tests..."
cargo test --workspace

echo "Check formatting..."
cargo fmt -- --check

echo "Run lints..."
cargo clippy -- -D warnings

echo "Check vulnerabilities..."
cargo audit

