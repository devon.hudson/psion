use super::matrix_overlay::behaviour::OverlayBehaviour;
use super::matrix_overlay::swarm::{OverlaySwarm, SwarmChannels};
use crate::domain::{
    keys,
    peer::PeerAddresses,
    router::{
        CommandReceiver, CommandSender, EventSender, Router, RouterCommand, RouterError,
        RouterExecutor, MAX_SOCKET_BUFFER_SIZE,
    },
    types::{SocketChannelPair, SocketData, SocketReceiver, SocketSender},
};
use crate::psion_libp2p::spanning_tree::behaviour::SpanningTree;
use crate::psion_libp2p::spanning_tree::handler::TreeConfig;
use crate::PeerAddress;
use async_trait::async_trait;
use ed25519_dalek::Keypair;
use futures::{channel::mpsc, FutureExt};
use libp2p::identify::{Identify, IdentifyConfig};
use libp2p::mdns::{Mdns, MdnsConfig};
use libp2p::Multiaddr;
use libp2p::{
    core::upgrade, identity::ed25519, mplex, plaintext::PlainText2Config, tcp::TcpConfig, Transport,
};
use log::{info, warn};
use std::error::Error;

/// Configuration parameters for the [`Libp2pRouter`].
#[derive(Debug)]
pub struct Libp2pRouterConfig {
    key_pair: Keypair,
}

/// [`Router`] implementation based on libp2p.
#[derive(Debug)]
pub struct Libp2pRouter {
    config: Libp2pRouterConfig,

    // Used to send `RouterEvent`s back up to the client application.
    event_channel: Option<EventSender>,

    // Used to send `RouterCommand`s down to the `MatrixOverlaySwarm`.
    command_channel: CommandSender,

    // Send/Recv channels for the [`OverlaySocket`].
    socket_tx: Option<SocketSender>,
    socket_rx: Option<SocketReceiver>,
}

impl Libp2pRouter {
    /// Spawns the background task of this router instance on the provided executor.
    fn start_background_task(
        &mut self,
        executor: impl RouterExecutor,
        key_pair: Keypair,
        local_public_key: libp2p::core::PublicKey,
        command_channel: CommandReceiver,
        listen_addresses: Vec<Multiaddr>,
        static_peers: Vec<Multiaddr>,
    ) {
        info!("Starting background task.");
        let executor_clone = executor.clone();
        let event_channel_clone = self.event_channel.clone();
        executor.exec(
            async move {
                let identify = Identify::new(IdentifyConfig::new(
                    // TODO : What string should I use here?
                    "p2p/1.0.0".to_string(),
                    local_public_key.clone(),
                ));
                // TODO : Handle error here.
                let mdns = Mdns::new(MdnsConfig::default()).await.unwrap();
                let tree = SpanningTree::new(TreeConfig::default(), key_pair);
                let swarm = OverlaySwarm::new(
                    executor_clone.clone(),
                    listen_addresses,
                    static_peers,
                    TcpConfig::new()
                        .upgrade(upgrade::Version::V1)
                        .authenticate(PlainText2Config {
                            local_public_key: local_public_key.clone(),
                        })
                        .multiplex(mplex::MplexConfig::default())
                        .boxed(),
                    OverlayBehaviour::new(identify, mdns, tree),
                    local_public_key,
                    SwarmChannels::new(command_channel, event_channel_clone),
                );

                executor_clone.exec(swarm.run().boxed());
            }
            .boxed(),
        );
    }
}

#[async_trait]
impl Router for Libp2pRouter {
    fn new(
        key_pair: keys::KeyPair,
        executor: impl RouterExecutor,
        listen_addresses: PeerAddresses,
        static_peers: PeerAddresses,
        event_channel: Option<EventSender>,
    ) -> Result<Self, RouterError> {
        let public_key = key_pair.1.to_vec();
        let local_public_key: libp2p::core::PublicKey =
            libp2p::core::PublicKey::Ed25519(match ed25519::PublicKey::decode(&public_key) {
                Ok(key) => key,
                Err(_) => {
                    warn!("Failed parsing public key into ed25519 format.");
                    return Err(RouterError::InvalidKey(
                        "Public key doesn't conform with ed25519".to_string(),
                    ));
                }
            });

        let dalek_key_pair = Keypair::from_bytes([key_pair.0, key_pair.1].concat().as_slice())?;

        let (command_tx, command_rx) = mpsc::channel(32);
        let config = Libp2pRouterConfig {
            key_pair: dalek_key_pair,
        };

        let mut router = Libp2pRouter {
            config,
            event_channel,
            command_channel: command_tx,
            socket_tx: None,
            socket_rx: None,
        };

        let listen_addresses = listen_addresses
            .iter()
            .flat_map(|peer_addr| convert_peer_to_multiaddr(peer_addr.clone()))
            .collect();

        let static_peers = static_peers
            .iter()
            .flat_map(|peer_addr| convert_peer_to_multiaddr(peer_addr.clone()))
            .collect();

        router.start_background_task(
            executor,
            Keypair::from_bytes([key_pair.0, key_pair.1].concat().as_slice())?,
            local_public_key,
            command_rx,
            listen_addresses,
            static_peers,
        );
        Ok(router)
    }

    fn get_overlay_socket_channels(&mut self) -> Result<SocketChannelPair, RouterError> {
        if self.socket_tx.is_some() || self.socket_rx.is_some() {
            warn!("Overlay Socket channels already exist for this router instance.");
            return Err(RouterError::OverlaySocketExists(
                "Overlay Socket channels already exist for this router instance.".to_string(),
            ));
        }

        let (router_tx, socket_rx) = mpsc::channel::<SocketData>(MAX_SOCKET_BUFFER_SIZE);
        let (socket_tx, router_rx) = mpsc::channel::<SocketData>(MAX_SOCKET_BUFFER_SIZE);

        self.socket_tx = Some(router_tx);
        self.socket_rx = Some(router_rx);

        Ok((socket_tx, socket_rx))
    }

    fn send_command(&mut self, command: RouterCommand) -> Result<(), RouterError> {
        info!("Received router command: {:?}", command);
        self.command_channel.try_send(command).map_err(|err| {
            if err.into_send_error().is_full() {
                RouterError::CommandChannelFull
            } else {
                RouterError::CommandChannelDisconnected
            }
        })
    }
}

fn convert_peer_to_multiaddr(peer_addr: PeerAddress) -> Result<Multiaddr, Box<dyn Error>> {
    match peer_addr {
        PeerAddress::SocketV4(addr) => {
            Ok(format!("/ip4/{}/tcp/{}", addr.ip(), addr.port()).parse()?)
        }
        PeerAddress::SocketV6(addr) => {
            Ok(format!("/ip6/{}/tcp/{}", addr.ip(), addr.port()).parse()?)
        }
        PeerAddress::RawString(addr) => Ok(addr.parse()?),
    }
}

#[cfg(test)]
mod tests {
    use std::{pin::Pin, time::Duration, vec};

    use super::*;
    use crate::domain::{
        keys::generate_node_keypair,
        peer::PeerAddress,
        router::{Router, RouterEvent},
    };
    use async_std::task;
    use futures::{
        future::{self, join_all},
        select, Future, StreamExt,
    };
    use futures_timer::Delay;
    use libp2p::multiaddr::Multiaddr;

    #[allow(dead_code)]
    fn enable_logging() {
        std::env::set_var("RUST_LOG", "debug");
        env_logger::init();
    }

    fn null_executor() -> impl RouterExecutor {
        |_fut| {}
    }

    fn async_executor() -> impl RouterExecutor {
        |fut| {
            task::spawn(fut);
        }
    }

    async fn join_or_timeout_and_panic<T>(
        futures: Vec<Pin<Box<dyn Future<Output = T> + '_>>>,
        timeout: Duration,
    ) -> Vec<T> {
        let process_futures = async { join_all(futures).await };
        let test_timeout = async { Delay::new(timeout).await };
        select! {
            future_results = process_futures.fuse() => future_results,
            _ = test_timeout.fuse() => panic!(),
        }
    }

    #[test]
    fn should_create_new_router_instance() {
        let router = Libp2pRouter::new(
            generate_node_keypair(),
            null_executor(),
            PeerAddresses::new(),
            PeerAddresses::new(),
            None,
        );
        assert!(router.is_ok());
    }

    #[test]
    fn should_create_new_socket_channels() {
        let mut router = Libp2pRouter::new(
            generate_node_keypair(),
            null_executor(),
            PeerAddresses::new(),
            PeerAddresses::new(),
            None,
        )
        .unwrap();

        let result = router.get_overlay_socket_channels();
        assert!(result.is_ok());
    }

    #[test]
    fn should_not_create_socket_channels_if_already_exist() {
        let mut router = Libp2pRouter::new(
            generate_node_keypair(),
            null_executor(),
            PeerAddresses::new(),
            PeerAddresses::new(),
            None,
        )
        .unwrap();

        let _ = router.get_overlay_socket_channels();
        let result = router.get_overlay_socket_channels();

        assert!(result.is_err());
    }

    #[async_std::test]
    async fn should_connect_to_provided_static_peers() {
        // Given: Two nodes are created with each other as known peers
        let (priv_key1, pub_key1) = generate_node_keypair();
        let rand_port = rand::random::<u64>().saturating_add(1);
        let peer1_addr: Multiaddr = format!("/memory/{}", rand_port).parse().unwrap();

        let (priv_key2, pub_key2) = generate_node_keypair();
        let rand_port = rand::random::<u64>().saturating_add(1);
        let peer2_addr: Multiaddr = format!("/memory/{}", rand_port).parse().unwrap();

        let (event_tx1, event_rx1) = mpsc::channel(32);
        let (event_tx2, event_rx2) = mpsc::channel(32);

        let _router1 = Libp2pRouter::new(
            (priv_key1, pub_key1),
            async_executor(),
            vec![PeerAddress::RawString(peer1_addr.to_string())],
            PeerAddresses::new(),
            Some(event_tx1),
        )
        .unwrap();
        let _router2 = Libp2pRouter::new(
            (priv_key2, pub_key2),
            async_executor(),
            vec![PeerAddress::RawString(peer2_addr.to_string())],
            vec![PeerAddress::RawString(peer1_addr.to_string())],
            Some(event_tx2),
        )
        .unwrap();

        // When: Both nodes are run
        // Then: Both nodes are connected to each other
        let mut filtered1 =
            event_rx1.filter(|ev| future::ready(matches!(ev, RouterEvent::ConnectionEstablished)));
        let mut filtered2 =
            event_rx2.filter(|ev| future::ready(matches!(ev, RouterEvent::ConnectionEstablished)));

        let _results = join_or_timeout_and_panic(
            vec![filtered1.next().boxed(), filtered2.next().boxed()],
            Duration::from_secs(3),
        )
        .await;
    }
}
