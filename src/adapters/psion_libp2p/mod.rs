/// `Router` implementation backed by libp2p.
pub mod router;
pub use router::Libp2pRouter;

/// The set of behaviours that define the matrix p2p overlay.
pub mod matrix_overlay;

/// The set of behaviours that define the spanning tree.
pub mod spanning_tree;
