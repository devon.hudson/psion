/// The set of behaviours that define the matrix p2p overlay.
pub mod behaviour;

/// An async task that runs the [`libp2p::swarm::Swarm`].
pub mod swarm;
