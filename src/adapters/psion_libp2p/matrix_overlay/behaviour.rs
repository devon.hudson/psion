use crate::psion_libp2p::spanning_tree::behaviour::{SpanningTree, TreeEvent};
use libp2p::{
    identify::{Identify, IdentifyEvent},
    mdns::{Mdns, MdnsEvent},
    swarm::{NetworkBehaviourAction, NetworkBehaviourEventProcess, PollParameters},
    NetworkBehaviour,
};
use std::{
    collections::VecDeque,
    fmt,
    task::{Context, Poll},
};

/// Out-event that may be returned when polling the Behaviour.
#[derive(Debug)]
pub enum BehaviourEvent {
    /// Identify Events from [`IdentifyEvent`].
    Identify(IdentifyEvent),
    /// Mdns Events from [`MdnsEvent`].
    Mdns(MdnsEvent),
    /// Spanning Tree Events from [`TreeEvent`].
    Tree(TreeEvent),
}

/// Set of behaviours that define the `Swarm`s behaviour.
#[derive(NetworkBehaviour)]
#[behaviour(out_event = "BehaviourEvent", poll_method = "poll")]
pub struct OverlayBehaviour {
    identify: Identify,
    mdns: Mdns,
    tree: SpanningTree,
    #[behaviour(ignore)]
    events: VecDeque<BehaviourEvent>,
}

impl OverlayBehaviour {
    /// Creates a new instance of this behavour.
    pub fn new(identify: Identify, mdns: Mdns, tree: SpanningTree) -> Self {
        Self {
            identify,
            mdns,
            tree,
            events: VecDeque::new(),
        }
    }

    // Method that is called when the swarm is polled.
    // If it returns Poll::Ready(event), the event is returned when polling swarm.next().
    fn poll<TEv>(
        &mut self,
        _cx: &mut Context<'_>,
        _params: &mut impl PollParameters,
    ) -> Poll<NetworkBehaviourAction<TEv, BehaviourEvent>> {
        if let Some(event) = self.events.pop_front() {
            return Poll::Ready(NetworkBehaviourAction::GenerateEvent(event));
        }

        Poll::Pending
    }
}

impl NetworkBehaviourEventProcess<IdentifyEvent> for OverlayBehaviour {
    fn inject_event(&mut self, event: IdentifyEvent) {
        if let IdentifyEvent::Received { peer_id, info } = &event {
            if let libp2p::core::PublicKey::Ed25519(key) = &info.public_key {
                self.tree.add_peer_key(peer_id, key.encode());
            }
        }

        self.events.push_back(BehaviourEvent::Identify(event));
    }
}

impl NetworkBehaviourEventProcess<MdnsEvent> for OverlayBehaviour {
    fn inject_event(&mut self, event: MdnsEvent) {
        self.events.push_back(BehaviourEvent::Mdns(event));
    }
}

impl NetworkBehaviourEventProcess<TreeEvent> for OverlayBehaviour {
    fn inject_event(&mut self, event: TreeEvent) {
        self.events.push_back(BehaviourEvent::Tree(event));
    }
}

impl fmt::Debug for OverlayBehaviour {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str("Boo")
    }
}
