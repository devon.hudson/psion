use super::behaviour::{BehaviourEvent, OverlayBehaviour};
use crate::{
    adapters::psion_libp2p::spanning_tree::behaviour::TreeEvent,
    domain::router::{CommandReceiver, EventSender, RouterEvent, MAX_PEER_CONNECTIONS},
    RouterExecutor,
};
use futures::{select, FutureExt, SinkExt, StreamExt};
use libp2p::{
    core::{muxing, transport},
    mdns::MdnsEvent,
    swarm::{SwarmBuilder, SwarmEvent},
    Multiaddr, PeerId, Swarm, Transport,
};
use log::{debug, error, info};
use std::fmt;

/// Channels used to communicate with the [`OverlayBehaviour`].
#[derive(Debug)]
pub struct SwarmChannels {
    command_channel: CommandReceiver,
    event_channel: Option<EventSender>,
}

impl SwarmChannels {
    /// Creates a new instance.
    pub fn new(command_channel: CommandReceiver, event_channel: Option<EventSender>) -> Self {
        Self {
            command_channel,
            event_channel,
        }
    }
}

/// The [Swarm][`libp2p::swarm::Swarm`] background task that drives the [Libp2pRouter][`crate::Libp2pRouter`].
pub struct OverlaySwarm {
    swarm: Swarm<OverlayBehaviour>,
    listen_addresses: Vec<Multiaddr>,
    static_peers: Vec<Multiaddr>,
    swarm_channels: SwarmChannels,
}

impl OverlaySwarm {
    // TODO : Refactor params into some for of config struct...
    /// Creates a new swarm with the desired behaviours.
    pub fn new(
        executor: impl RouterExecutor,
        listen_addresses: Vec<Multiaddr>,
        static_peers: Vec<Multiaddr>,
        transport: transport::Boxed<(PeerId, muxing::StreamMuxerBox)>,
        behaviour: OverlayBehaviour,
        local_public_key: libp2p::core::PublicKey,
        swarm_channels: SwarmChannels,
    ) -> Self {
        OverlaySwarm::create_swarm(
            executor,
            listen_addresses,
            static_peers,
            transport,
            behaviour,
            local_public_key,
            swarm_channels,
        )
    }

    #[cfg(not(test))]
    fn create_swarm(
        executor: impl RouterExecutor,
        listen_addresses: Vec<Multiaddr>,
        static_peers: Vec<Multiaddr>,
        transport: transport::Boxed<(PeerId, muxing::StreamMuxerBox)>,
        behaviour: OverlayBehaviour,
        local_public_key: libp2p::core::PublicKey,
        swarm_channels: SwarmChannels,
    ) -> Self {
        OverlaySwarm {
            swarm: SwarmBuilder::new(
                transport.boxed(),
                behaviour,
                PeerId::from_public_key(local_public_key),
            )
            .executor(Box::new(executor))
            .build(),
            listen_addresses,
            static_peers,
            swarm_channels,
        }
    }

    #[cfg(test)]
    fn create_swarm(
        executor: impl RouterExecutor,
        listen_addresses: Vec<Multiaddr>,
        static_peers: Vec<Multiaddr>,
        _transport: transport::Boxed<(PeerId, muxing::StreamMuxerBox)>,
        behaviour: OverlayBehaviour,
        local_public_key: libp2p::core::PublicKey,
        swarm_channels: SwarmChannels,
    ) -> Self {
        use libp2p::{core::transport::MemoryTransport, mplex, plaintext::PlainText2Config};

        OverlaySwarm {
            swarm: SwarmBuilder::new(
                MemoryTransport::default()
                    .upgrade(libp2p::core::upgrade::Version::V1)
                    .authenticate(PlainText2Config {
                        local_public_key: local_public_key.clone(),
                    })
                    .multiplex(mplex::MplexConfig::default())
                    .boxed(),
                behaviour,
                PeerId::from_public_key(local_public_key),
            )
            .executor(Box::new(executor))
            .build(),
            listen_addresses,
            static_peers,
            swarm_channels,
        }
    }

    /// Main task for processing the [Swarm][`libp2p::swarm::Swarm`].
    /// - Drive the `Swarm` by polling it for events.
    /// - Poll the commands channel for commands that are dents from [`crate::Libp2pRouter`].
    pub async fn run(mut self) {
        self.listen_addresses.clone().iter().for_each(|addr| {
            info!("Listening on {:?}", addr.clone());
            if let Err(err) = self.swarm.listen_on(addr.clone()) {
                error!("Failed listening on {:?}", err);
            };
        });
        self.static_peers.clone().iter().for_each(|addr| {
            info!("Dialing {:?}", addr.clone());
            if let Err(err) = self.swarm.dial_addr(addr.clone()) {
                error!("Failed dialing {:?}", err);
            };
        });

        loop {
            select! {
                event = self.swarm.select_next_some() => {
                    debug!("Got an event! {:?}", event);

                    // TODO : Create a swarm event handler function to dispatch to.
                    match event {
                        SwarmEvent::ConnectionEstablished {peer_id: _, endpoint, ..} => {
                            info!("Connected to {}", endpoint.get_remote_address());
                            if let Some(ref mut ch) = self.swarm_channels.event_channel {
                                if let Err(err) = ch.send(RouterEvent::ConnectionEstablished).await {
                                    error!("Error sending into event channel: {:?}", err);
                                };
                            }
                        },
                        SwarmEvent::ConnectionClosed {..} => {},
                        SwarmEvent::Behaviour(BehaviourEvent::Mdns(MdnsEvent::Discovered(peers))) => {
                            info!("Mdns Event!");
                            peers.for_each(|peer| {
                                info!("Found peer {:?}\nADDR: {}", peer, peer.1);
                                if !self.swarm.is_connected(&peer.0) && self.swarm.network_info().num_peers() < MAX_PEER_CONNECTIONS {
                                    if let Err(err) = self.swarm.dial_addr(peer.1) {
                                        error!("Failed dialing {:?}", err);
                                    }
                                }
                            });
                        },
                        SwarmEvent::Behaviour(BehaviourEvent::Tree(TreeEvent{ root_key, coordinates, parent } )) => {
                            if let Some(ref mut ch) = self.swarm_channels.event_channel {
                                if let Err(err) = ch.send(RouterEvent::NewSpanningTreeRoot{
                                    root_key, coordinates, parent
                                }).await {
                                    error!("Error sending into event channel: {:?}", err);
                                };
                            }
                        },
                        _ => {},
                    }
                },
                command = self.swarm_channels.command_channel.next().fuse() => {
                    if let Some(_c) = command {
                    } else {
                        // Shutdown the task if the command channel has been closed.
                        break;
                    }
                },
            }
        }
    }
}

impl fmt::Debug for OverlaySwarm {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str("Boo")
    }
}
