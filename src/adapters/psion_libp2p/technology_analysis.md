# Libp2p Technology Analysis
## Overview
Libp2p has a particular way of communicating between peers and amongst the various protocols running between those peers.
Psion-libp2p is an attempt to map the matrix p2p systems onto the libp2p concepts.

The rust libp2p library commonly breaks protocols out into three different pieces. These are the behaviour, the protocol, and the protocol handler.
The behaviour is a single future that is run for all peer streams communicating with this protocol.
The protocol is the actual exchange of protocol specific messages over the peer streams.
The protocol handler is a future per peer connection that deals with sending and receiving the various protocol messages.
To communicate between the protocol and the behaviour, events are used to bubble up certain actions. To communicate down to the protocol handlers, the behaviour generates handler notifications for the appropriate peer connection.

To map the matrix p2p systems onto libp2p it was simply a matter of splitting out the information needed to communicate between two peers and the information needed a layer higher to handle the aggregation of all connected peers.

## Matrix P2P Design
### Spanning Tree
* Single top-level behaviour for all peers
  * Contains the "peer switch"
* Protocol handler per peer connection
  * Handles sending and receiving root announcements as well as de/serialization
  
### Matrix Overlay
* Composed of all the different protocols used to create the matrix p2p network (mdns, spanning tree, virtual snake, etc.)
* Aggregates events that propagate out of each running sub-protocol
* Acts as message broker between the various protocols
* Will create the "overlay socket" that external applications can use to send/recv data over the p2p network
* Emits events up to the external application that may be of interest

## Pros of Integrating with Libp2p
* Leverages existing libp2p transport stack
* Can leverage the libp2p transport encryption upgrades
* Leverages the libp2p mdns peer discovery protocol
* Adhering to the libp2p concepts reinforces clean decoupling between the various systems within the matrix p2p overlay

## Cons of Integrating with Libp2p
* Requires using PeerIds (hashed public keys) to establish peer connections (May not be a con, but currently matrix p2p relies solely on public keys)
* Cannot easily interoperate between libp2p and non-libp2p nodes due to the specific way libp2p establishes and upgrades peer connections
* Additional traffic overhead required to "upgrade" and negotiate peer capabilities within the libp2p system

## Conclusion
Psion-libp2p is a working example of how the matrix p2p overlay network could be integrated with libp2p.
There are many benefits to integrating with libp2p, mainly in leveraging their existing message brokering and peer discovery systems, but the added overhead may not be worth the cost.

After going through the effort to integrate with libp2p, it is clear that the choice to integrate will need to be made early on in matrix p2p development.
Integrating with libp2p after matrix has an established p2p protocol will be very difficult. 

The way libp2p handles peer connection setup and upgrading, it would be non-trivial to bridge this with the way matrix p2p handles new peer connections.
Further, choosing to integrate with libp2p later in time would most likely eliminate the ability to structure the protocols as I have done here since libp2p requires establishing these protocols via a series of upgrade messages between peers.
Without the ability to structure the matrix p2p library to take advantage of libp2p's design, there is not much benefit in performing the integration.

Lastly, due to the added overhead to negotiate libp2p protocols between peers and the requirements on using PeerIds for identification, it may be undesirable to integrate with libp2p for these reasons alone.
I don't have specific numbers on what the actual overhead is at this time, other than it is non-zero.
