use super::handler::{TreeConfig, TreeHandler, TreeResult};
use crate::domain::{
    keys::PublicKey,
    spanning_tree::{
        announcement::RootAnnouncement,
        node::{AnnouncementResult, Coordinates, Node},
        switch::{PeerState, ROOT_ANNOUNCEMENT_INTERVAL, ROOT_ANNOUNCEMENT_TIMEOUT},
    },
};
use ed25519_dalek::Keypair;
use futures::FutureExt;
use futures_timer::Delay;
use libp2p::{
    core::connection::ConnectionId,
    swarm::{
        IntoProtocolsHandler, NetworkBehaviour, NetworkBehaviourAction, NotifyHandler,
        ProtocolsHandler,
    },
    PeerId,
};
use log::{debug, error, info};
use std::{
    collections::{HashMap, VecDeque},
    task::Poll,
    time::Duration,
};

type NetworkAction<Proto> = NetworkBehaviourAction<
    <<<Proto as NetworkBehaviour>::ProtocolsHandler as IntoProtocolsHandler>::Handler as ProtocolsHandler>::InEvent,
    <Proto as NetworkBehaviour>::OutEvent,
>;

type PublicKeyMap = HashMap<PeerId, PublicKey>;

#[derive(Clone, Copy, Debug)]
struct PeerInfo {
    peer_id: PeerId,
    conn_id: ConnectionId,
}

impl PartialEq for PeerInfo {
    fn eq(&self, other: &Self) -> bool {
        self.peer_id == other.peer_id
    }
}

#[derive(Debug)]
enum InternalTreeEvent {
    TreeStarted,
    AnnouncementReceived { peer_id: PeerId, result: TreeResult },
}

/// And event that can be emitted from the spanning tree.
#[derive(Debug, PartialEq)]
pub struct TreeEvent {
    /// The newly accepted root announcement.
    pub root_key: PublicKey,
    /// The new coordinates for this node.
    pub coordinates: Coordinates,
    /// This node's parent's key.
    pub parent: Option<PublicKey>,
}

/// A `NetworkBehaviour` for the matrix spanning tree.
#[derive(Debug)]
pub struct SpanningTree {
    config: TreeConfig,
    root_timer: Option<Delay>,   // Only used if this node is the root.
    announcement_timeout: Delay, // Triggers if root announcements haven't been seen.
    public_key_map: PublicKeyMap,
    tree_node: Node<PeerInfo>,
    events: VecDeque<InternalTreeEvent>,
    protocol_tx: VecDeque<NetworkBehaviourAction<RootAnnouncement, TreeEvent>>,
}

impl SpanningTree {
    /// Create a new spanning tree behaviour instance.
    pub fn new(config: TreeConfig, key_pair: Keypair) -> Self {
        SpanningTree {
            config,
            root_timer: Some(Delay::new(Duration::from_secs(ROOT_ANNOUNCEMENT_INTERVAL))),
            announcement_timeout: Delay::new(Duration::from_secs(ROOT_ANNOUNCEMENT_TIMEOUT)),
            public_key_map: PublicKeyMap::new(),
            tree_node: Node::new(key_pair),
            events: VecDeque::from(vec![InternalTreeEvent::TreeStarted]),
            protocol_tx: VecDeque::new(),
        }
    }

    /// Informs the spanning tree of a new peer_id to public key mapping.
    pub fn add_peer_key(&mut self, peer_id: &PeerId, key: PublicKey) {
        self.public_key_map.insert(*peer_id, key);
    }

    fn generate_root_advertisements(&mut self) {
        info!("Sending new root announcement");
        // Any previously unsent root announcements are no longer valid.
        self.protocol_tx.clear();

        // Generate a new root announcement for each connected peer.
        let mut queue = Vec::new();
        self.tree_node
            .switch
            .iter()
            .enumerate()
            .filter(|(_, peer)| peer.is_some())
            .for_each(|(i, peer)| {
                let announcement = self
                    .tree_node
                    .generate_announcement(peer.as_ref().unwrap(), i as u64);
                queue.push(NetworkBehaviourAction::NotifyHandler {
                    event: announcement,
                    handler: NotifyHandler::One(peer.as_ref().unwrap().id.conn_id),
                    peer_id: peer.as_ref().unwrap().id.peer_id,
                });
            });

        queue
            .into_iter()
            .for_each(|i| self.protocol_tx.push_front(i));
    }

    fn handle_root_timeout(&mut self) {
        self.announcement_timeout
            .reset(Duration::from_secs(ROOT_ANNOUNCEMENT_TIMEOUT));
        info!(
            "No root announcements received within {} seconds, selecting new parent.",
            ROOT_ANNOUNCEMENT_TIMEOUT
        );

        debug!("Connected peers: \n{:?}", self.tree_node.switch);
        self.tree_node.select_new_parent();

        match self.tree_node.parent_switch_port {
            Some(_) => self.root_timer = None,
            None => {
                self.root_timer = Some(Delay::new(Duration::from_secs(ROOT_ANNOUNCEMENT_INTERVAL)))
            }
        }
    }

    fn handle_root_responsibilities(&mut self) {
        self.announcement_timeout
            .reset(Duration::from_secs(ROOT_ANNOUNCEMENT_TIMEOUT));

        if let Some(root_timer) = self.root_timer.as_mut() {
            root_timer.reset(Duration::from_secs(ROOT_ANNOUNCEMENT_INTERVAL));
        }

        self.generate_root_advertisements();
    }

    fn get_current_node_state(&self) -> TreeEvent {
        match &self.tree_node.root_info {
            Some(root_info) => {
                // TODO : What is the best way to store/access the parent key here?
                let mut parent = None;
                if let Some((_last, elements)) = root_info.signatures.split_last() {
                    if let Some(item) = elements.last() {
                        parent = Some(item.public_key);
                    }
                }

                TreeEvent {
                    root_key: root_info.root_key,
                    coordinates: self.tree_node.coordinates.clone(),
                    parent,
                }
            }
            None => TreeEvent {
                root_key: self.tree_node.key_pair.public.to_bytes(),
                coordinates: self.tree_node.coordinates.clone(),
                parent: None,
            },
        }
    }

    fn handle_announcement(&mut self, announcement: &RootAnnouncement, peer_id: &PeerId) {
        if let Ok(coordinate) = self.tree_node.switch.get_peer_coordinate(&PeerState {
            id: PeerInfo {
                peer_id: *peer_id,
                conn_id: ConnectionId::new(0),
            },
            last_announcement: None,
        }) {
            let mut forward_to_peers = false;

            match self.tree_node.handle_announcement(announcement, coordinate) {
                AnnouncementResult::RootUpdated => {
                    info!("Updated root info based on announcement");
                    self.announcement_timeout
                        .reset(Duration::from_secs(ROOT_ANNOUNCEMENT_TIMEOUT));
                    forward_to_peers = true;
                }
                AnnouncementResult::RootAndParentUpdated => {
                    info!("Updated parent and root info based on announcement");
                    self.announcement_timeout
                        .reset(Duration::from_secs(ROOT_ANNOUNCEMENT_TIMEOUT));
                    forward_to_peers = true;
                }
                AnnouncementResult::Dropped => {}
            }

            if forward_to_peers {
                self.generate_root_advertisements();
            }
        } else {
            // TODO : Handle this case better! Maybe disconnect peer?
            error!("Cannot find peer in switch...");
            return;
        }

        // NOTE : We aren't root so don't run the root timer.
        if self.tree_node.parent_switch_port.is_some() {
            self.root_timer = None
        }
    }
}

impl NetworkBehaviour for SpanningTree {
    type ProtocolsHandler = TreeHandler;
    type OutEvent = TreeEvent;

    fn new_handler(&mut self) -> Self::ProtocolsHandler {
        TreeHandler::new(self.config.clone())
    }

    fn addresses_of_peer(&mut self, _peer_id: &libp2p::PeerId) -> Vec<libp2p::Multiaddr> {
        Vec::new()
    }

    fn inject_connected(&mut self, _peer_id: &libp2p::PeerId) {}

    fn inject_disconnected(&mut self, _peer_id: &libp2p::PeerId) {}

    fn inject_event(
        &mut self,
        peer_id: libp2p::PeerId,
        _connection: libp2p::core::connection::ConnectionId,
        result: TreeResult,
    ) {
        self.events
            .push_front(InternalTreeEvent::AnnouncementReceived { peer_id, result });
    }

    fn poll(
        &mut self,
        cx: &mut std::task::Context<'_>,
        _params: &mut impl libp2p::swarm::PollParameters,
    ) -> Poll<NetworkAction<Self>> {
        if let Poll::Ready(()) = self.announcement_timeout.poll_unpin(cx) {
            self.handle_root_timeout();
        }

        if let Some(root_timer) = self.root_timer.as_mut() {
            if let Poll::Ready(()) = root_timer.poll_unpin(cx) {
                self.handle_root_responsibilities();
            }
        }

        if let Some(event) = self.events.pop_back() {
            match event {
                InternalTreeEvent::AnnouncementReceived { peer_id, result } => {
                    if let Ok(announcement) = result {
                        info!("Received announcement: {:?}", announcement);
                        let old_state = self.get_current_node_state();
                        self.handle_announcement(&announcement, &peer_id);
                        let new_state = self.get_current_node_state();

                        if old_state != new_state {
                            return Poll::Ready(NetworkBehaviourAction::GenerateEvent(new_state));
                        }
                    }
                }
                InternalTreeEvent::TreeStarted => {
                    return Poll::Ready(NetworkBehaviourAction::GenerateEvent(TreeEvent {
                        root_key: self.tree_node.key_pair.public.to_bytes(),
                        coordinates: Coordinates::default(),
                        parent: None,
                    }));
                }
            }
        }

        if let Some(event) = self.protocol_tx.pop_back() {
            return Poll::Ready(event);
        }

        Poll::Pending
    }

    fn inject_connection_established(
        &mut self,
        peer_id: &libp2p::PeerId,
        conn_id: &libp2p::core::connection::ConnectionId,
        _: &libp2p::core::ConnectedPoint,
    ) {
        info!("New peer connected, adding to tree...");
        let peer_state = PeerState {
            id: PeerInfo {
                peer_id: *peer_id,
                conn_id: *conn_id,
            },
            last_announcement: None,
        };
        let result = self.tree_node.switch.add_peer(&peer_state);

        if let Ok(port) = result {
            // Forward our latest root announcement to the new node
            let event = NetworkBehaviourAction::NotifyHandler {
                event: self.tree_node.generate_announcement(&peer_state, port),
                handler: NotifyHandler::One(*conn_id),
                peer_id: *peer_id,
            };
            self.protocol_tx.push_front(event);
        }
    }

    fn inject_connection_closed(
        &mut self,
        peer_id: &libp2p::PeerId,
        conn_id: &libp2p::core::connection::ConnectionId,
        _: &libp2p::core::ConnectedPoint,
    ) {
        info!("Peer connection closed, removing from tree...");
        self.tree_node.switch.remove_peer(&PeerState {
            id: PeerInfo {
                peer_id: *peer_id,
                conn_id: *conn_id,
            },
            last_announcement: None,
        });
        self.public_key_map.remove(peer_id);
    }

    fn inject_address_change(
        &mut self,
        _: &libp2p::PeerId,
        _: &libp2p::core::connection::ConnectionId,
        _old: &libp2p::core::ConnectedPoint,
        _new: &libp2p::core::ConnectedPoint,
    ) {
    }

    fn inject_addr_reach_failure(
        &mut self,
        _peer_id: Option<&libp2p::PeerId>,
        _addr: &libp2p::Multiaddr,
        _error: &dyn std::error::Error,
    ) {
    }

    fn inject_dial_failure(&mut self, _peer_id: &libp2p::PeerId) {}

    fn inject_new_listener(&mut self, _id: libp2p::core::connection::ListenerId) {}

    fn inject_new_listen_addr(
        &mut self,
        _id: libp2p::core::connection::ListenerId,
        _addr: &libp2p::Multiaddr,
    ) {
    }

    fn inject_expired_listen_addr(
        &mut self,
        _id: libp2p::core::connection::ListenerId,
        _addr: &libp2p::Multiaddr,
    ) {
    }

    fn inject_listener_error(
        &mut self,
        _id: libp2p::core::connection::ListenerId,
        _err: &(dyn std::error::Error + 'static),
    ) {
    }

    fn inject_listener_closed(
        &mut self,
        _id: libp2p::core::connection::ListenerId,
        _reason: Result<(), &std::io::Error>,
    ) {
    }

    fn inject_new_external_addr(&mut self, _addr: &libp2p::Multiaddr) {}

    fn inject_expired_external_addr(&mut self, _addr: &libp2p::Multiaddr) {}
}

#[cfg(test)]
mod tests {
    #[test]
    fn should_start_as_root_node() {
        // todo!()
    }

    // #[test]
    // fn should_negotiate_same_root_between_multiple_nodes() {
    //     todo!()
    // }

    // #[test]
    // fn should_negotiate_same_root_between_multiple_nodes_scaled_up() {
    //     todo!()
    // }

    // #[test]
    // fn should_negotiate_same_root_after_merging_trees() {
    //     todo!()
    // }
}
