use crate::domain::{
    frame::{Frame, FRAME_HEADER_SIZE},
    spanning_tree::announcement::RootAnnouncement,
};
use bytes::Bytes;
use futures::{future, AsyncRead, AsyncReadExt, AsyncWrite, AsyncWriteExt};
use libp2p::{core::UpgradeInfo, swarm::NegotiatedSubstream, InboundUpgrade, OutboundUpgrade};
use std::{
    io::{self},
    iter,
};
use void::Void;

/// The protocol for the spanning tree.
#[derive(Default, Debug, Copy, Clone)]
pub struct TreeProtocol;

impl UpgradeInfo for TreeProtocol {
    type Info = &'static [u8];
    type InfoIter = iter::Once<Self::Info>;

    fn protocol_info(&self) -> Self::InfoIter {
        iter::once(b"/matrix/tree/1.0.0")
    }
}

impl InboundUpgrade<NegotiatedSubstream> for TreeProtocol {
    type Output = NegotiatedSubstream;
    type Error = Void;
    type Future = future::Ready<Result<Self::Output, Self::Error>>;

    fn upgrade_inbound(self, stream: NegotiatedSubstream, _: Self::Info) -> Self::Future {
        future::ok(stream)
    }
}

impl OutboundUpgrade<NegotiatedSubstream> for TreeProtocol {
    type Output = NegotiatedSubstream;
    type Error = Void;
    type Future = future::Ready<Result<Self::Output, Self::Error>>;

    fn upgrade_outbound(self, stream: NegotiatedSubstream, _: Self::Info) -> Self::Future {
        future::ok(stream)
    }
}

/// Sends a root announcement.
pub async fn send_root_announcement<S>(
    mut stream: S,
    announcement: RootAnnouncement,
) -> io::Result<S>
where
    S: AsyncRead + AsyncWrite + Unpin,
{
    let payload = announcement.serialize();
    log::debug!("Preparing announcement payload {:x?}", payload);
    stream.write_all(&payload).await?;
    stream.flush().await?;

    Ok(stream)
}

/// Listens for incoming root announcements.
pub async fn recv_root_announcement<S>(mut stream: S) -> io::Result<(S, RootAnnouncement)>
where
    S: AsyncRead + AsyncWrite + Unpin,
{
    log::debug!("Listening for announcements ...");

    let mut frame_header = vec![0u8; FRAME_HEADER_SIZE];
    stream.read_exact(&mut frame_header).await?;
    log::debug!("Received frame header: {:x?}", frame_header);
    let mut frame_header = Bytes::from(frame_header);

    let data_length = Frame::deserialize(&mut frame_header)?.len();

    let mut payload = vec![0u8; data_length as usize];
    stream.read_exact(&mut payload).await?;
    log::debug!("Received payload: {:x?}", payload);
    let mut payload = Bytes::from(payload);

    let announcement = RootAnnouncement::deserialize(&mut payload)?;

    Ok((stream, announcement))
}

#[cfg(test)]
mod tests {
    #[allow(unused_imports)]
    use super::*;
    use crate::domain::{keys::SIGNATURE_LENGTH, spanning_tree::announcement::SignatureWithHop};
    use futures::{FutureExt, StreamExt};
    use libp2p::{
        core::multiaddr::multiaddr,
        core::transport::{ListenerEvent, MemoryTransport},
        Transport,
    };
    use rand::{thread_rng, Rng};
    use std::time::{Instant, SystemTime, UNIX_EPOCH};

    fn generate_announcement() -> RootAnnouncement {
        RootAnnouncement {
            root_key: [
                0xff, 0xff, 0xff, 0xff, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1, 0x1, 0x1, 0x2, 0x2, 0x2, 0x2,
                0x3, 0x3, 0x3, 0x3, 0x4, 0x4, 0x4, 0x4, 0x5, 0x5, 0x5, 0x5, 0x6, 0x6, 0x6, 0x6,
            ],
            sequence: SystemTime::now()
                .duration_since(UNIX_EPOCH)
                .unwrap()
                .as_nanos() as u64,
            signatures: vec![SignatureWithHop {
                hop: 1,
                public_key: [
                    0xff, 0xff, 0xff, 0xff, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1, 0x1, 0x1, 0x2, 0x2, 0x2,
                    0x2, 0x3, 0x3, 0x3, 0x3, 0x4, 0x4, 0x4, 0x4, 0x5, 0x5, 0x5, 0x5, 0x6, 0x6, 0x6,
                    0x6,
                ],
                // TODO : generate proper signature here.
                signature: [0; SIGNATURE_LENGTH],
            }],
            timestamp: Instant::now(),
        }
    }

    #[test]
    fn should_recv_announcement_sent_by_peer() {
        // Given: two peers are connected
        let mem_addr = multiaddr![Memory(thread_rng().gen::<u64>())];
        let mut listener = MemoryTransport.listen_on(mem_addr).unwrap();

        let listener_addr =
            if let Some(Some(Ok(ListenerEvent::NewAddress(a)))) = listener.next().now_or_never() {
                a
            } else {
                panic!("MemoryTransport not listening on an address!");
            };

        // When: an announcement is sent from one peer
        let announcement = generate_announcement();
        let ann_clone = announcement.clone();

        let result = async_std::task::spawn(async move {
            let listener_event = listener.next().await.unwrap();
            let (listener_upgrade, _) = listener_event.unwrap().into_upgrade().unwrap();
            let conn = listener_upgrade.await.unwrap();
            recv_root_announcement(conn).await.unwrap().1
        });

        // Then: the same announcement is read by the receiving peer
        async_std::task::spawn(async move {
            let c = MemoryTransport.dial(listener_addr).unwrap().await.unwrap();
            send_root_announcement(c, ann_clone).await.unwrap();
        });

        async_std::task::block_on(async move {
            let result = result.await;
            assert_eq!(result.root_key, announcement.root_key);
            assert_eq!(result.sequence, announcement.sequence);
            assert_eq!(result.signatures, announcement.signatures);
        })
    }
}
