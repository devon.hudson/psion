use core::fmt;
use std::{
    collections::VecDeque,
    error::Error,
    fmt::Debug,
    io,
    num::NonZeroU32,
    task::{Context, Poll},
};

use futures::{future::BoxFuture, FutureExt};
use libp2p::{
    core::{upgrade::NegotiationError, UpgradeError},
    swarm::{
        KeepAlive, NegotiatedSubstream, ProtocolsHandler, ProtocolsHandlerEvent,
        ProtocolsHandlerUpgrErr, SubstreamProtocol,
    },
};
use log::info;
use void::Void;

use crate::domain::spanning_tree::announcement::RootAnnouncement;

use super::protocol::{self, TreeProtocol};

/// The configuration for the spanning tree.
#[derive(Clone, Debug)]
pub struct TreeConfig {
    /// The maximum number of failed outbound messages before the associated
    /// connection is deemed unhealthy, indicating to the `Swarm` that it
    /// should be closed.
    max_failures: NonZeroU32,
    /// Whether the connection should generally be kept alive unless
    /// `max_failures` occur.
    keep_alive: bool,
}

impl TreeConfig {
    /// Creates a new `TreeConfig` with the following default settings:
    ///
    ///   * [`TreeConfig::with_max_failures`] 1
    ///   * [`TreeConfig::with_keep_alive`] true
    pub fn new() -> Self {
        Self {
            max_failures: NonZeroU32::new(1).expect("1 != 0"),
            keep_alive: true,
        }
    }

    /// Sets the maximum number of consecutive failures upon which the remote
    /// peer is considered unreachable and the connection closed.
    pub fn with_max_failures(mut self, n: NonZeroU32) -> Self {
        self.max_failures = n;
        self
    }

    /// Sets whether the tree protocol itself should keep the connection alive,
    /// apart from the maximum allowed failures.
    pub fn with_keep_alive(mut self, b: bool) -> Self {
        self.keep_alive = b;
        self
    }
}

impl Default for TreeConfig {
    fn default() -> Self {
        Self::new()
    }
}

/// The result of an inbound announcement.
pub type TreeResult = Result<RootAnnouncement, TreeFailure>;

/// An outbound tree failure.
#[derive(Debug)]
pub enum TreeFailure {
    /// The message timed out, i.e. no response was received within the
    /// configured timeout.
    Timeout,
    /// The peer does not support the tree protocol.
    Unsupported,
    /// The message failed for reasons other than a timeout.
    Other {
        /// Generic error returned during tree protocol handling.
        error: Box<dyn std::error::Error + Send + 'static>,
    },
}

impl fmt::Display for TreeFailure {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            TreeFailure::Timeout => f.write_str("Tree timeout"),
            TreeFailure::Other { error } => write!(f, "Tree error: {}", error),
            TreeFailure::Unsupported => write!(f, "Tree protocol not supported"),
        }
    }
}

impl Error for TreeFailure {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match self {
            TreeFailure::Timeout => None,
            TreeFailure::Other { error } => Some(&**error),
            TreeFailure::Unsupported => None,
        }
    }
}

/// Protocol handler that handles inbound and outbound spanning tree messages.
pub struct TreeHandler {
    /// Configuration options.
    config: TreeConfig,
    /// Outbound message failures that are pending to be processed by `poll()`.
    pending_errors: VecDeque<TreeFailure>,
    /// The number of consecutive failures that occurred.
    ///
    /// Each successful announcement resets this counter to 0.
    failures: u32,
    /// The outbound tree state.
    outbound: Option<TreeState>,
    /// The inbound announcement handler, i.e. if there is an inbound
    /// substream, this is always a future that waits for the
    /// next inbound announcement to be answered.
    inbound: Option<RecvFuture>,
    /// Tracks the state of our handler.
    state: State,
    /// The announcement to send out.
    announcement: Option<RootAnnouncement>,
}

impl Debug for TreeHandler {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str("TreeHandler")
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum State {
    /// We are inactive because the other peer doesn't support spanning tree.
    Inactive {
        /// Whether or not we've reported the missing support yet.
        ///
        /// This is used to avoid repeated events being emitted for a specific connection.
        reported: bool,
    },
    /// We are actively messaging the other peer.
    Active,
}

impl TreeHandler {
    /// Builds a new `TreeHandler` with the given configuration.
    pub fn new(config: TreeConfig) -> Self {
        TreeHandler {
            config,
            pending_errors: VecDeque::with_capacity(2),
            failures: 0,
            outbound: None,
            inbound: None,
            state: State::Active,
            announcement: None,
        }
    }
}

/// Incoming event type for the spanning tree protocol handler.
pub type TreeHandlerEvent = RootAnnouncement;

impl ProtocolsHandler for TreeHandler {
    type InEvent = TreeHandlerEvent;
    type OutEvent = TreeResult;
    type Error = TreeFailure;
    type InboundProtocol = TreeProtocol;
    type OutboundProtocol = TreeProtocol;
    type OutboundOpenInfo = ();
    type InboundOpenInfo = ();

    fn listen_protocol(&self) -> SubstreamProtocol<TreeProtocol, ()> {
        SubstreamProtocol::new(TreeProtocol, ())
    }

    fn inject_fully_negotiated_inbound(&mut self, stream: NegotiatedSubstream, (): ()) {
        self.inbound = Some(protocol::recv_root_announcement(stream).boxed());
    }

    fn inject_fully_negotiated_outbound(&mut self, stream: NegotiatedSubstream, (): ()) {
        self.outbound = Some(TreeState::Idle(stream));
    }

    fn inject_event(&mut self, event: Self::InEvent) {
        info!("New announcement to send");
        self.announcement = Some(event);
    }

    fn inject_dial_upgrade_error(&mut self, _info: (), error: ProtocolsHandlerUpgrErr<Void>) {
        self.outbound = None; // Request a new substream on the next `poll`.

        let error = match error {
            ProtocolsHandlerUpgrErr::Upgrade(UpgradeError::Select(NegotiationError::Failed)) => {
                debug_assert_eq!(self.state, State::Active);

                self.state = State::Inactive { reported: false };
                return;
            }
            // Note: This timeout only covers protocol negotiation.
            ProtocolsHandlerUpgrErr::Timeout => TreeFailure::Timeout,
            e => TreeFailure::Other { error: Box::new(e) },
        };

        self.pending_errors.push_front(error);
    }

    fn connection_keep_alive(&self) -> KeepAlive {
        if self.config.keep_alive {
            KeepAlive::Yes
        } else {
            KeepAlive::No
        }
    }

    fn poll(
        &mut self,
        cx: &mut Context<'_>,
    ) -> Poll<ProtocolsHandlerEvent<TreeProtocol, (), TreeResult, Self::Error>> {
        match self.state {
            State::Inactive { reported: true } => {
                return Poll::Pending; // nothing to do on this connection
            }
            State::Inactive { reported: false } => {
                self.state = State::Inactive { reported: true };
                return Poll::Ready(ProtocolsHandlerEvent::Custom(Err(TreeFailure::Unsupported)));
            }
            State::Active => {}
        }

        // Respond to inbound announcements.
        if let Some(fut) = self.inbound.as_mut() {
            match fut.poll_unpin(cx) {
                Poll::Pending => {}
                Poll::Ready(Err(e)) => {
                    log::debug!("Inbound announcement error: {:?}", e);
                    self.inbound = None;
                    self.pending_errors
                        .push_front(TreeFailure::Other { error: Box::new(e) });
                }
                Poll::Ready(Ok((stream, announcement))) => {
                    // An announcement from a remote peer has been received, wait for the next.
                    self.inbound = Some(protocol::recv_root_announcement(stream).boxed());
                    return Poll::Ready(ProtocolsHandlerEvent::Custom(Ok(announcement)));
                }
            }
        }

        loop {
            // Check for stream failures.
            if let Some(error) = self.pending_errors.pop_back() {
                log::debug!("Tree failure: {:?}", error);

                self.failures += 1;

                if self.failures >= self.config.max_failures.get() {
                    log::debug!("Too many failures ({}). Closing connection.", self.failures);
                    return Poll::Ready(ProtocolsHandlerEvent::Close(error));
                }

                return Poll::Ready(ProtocolsHandlerEvent::Custom(Err(error)));
            }

            // Continue outbound announcements.
            match self.outbound.take() {
                Some(TreeState::Advertise(mut announcement)) => match announcement.poll_unpin(cx) {
                    Poll::Pending => {
                        self.outbound = Some(TreeState::Advertise(announcement));
                        break;
                    }
                    Poll::Ready(Ok(stream)) => {
                        self.failures = 0;
                        self.outbound = Some(TreeState::Idle(stream));
                    }
                    Poll::Ready(Err(e)) => {
                        self.pending_errors
                            .push_front(TreeFailure::Other { error: Box::new(e) });
                    }
                },
                Some(TreeState::Idle(stream)) => {
                    match self.announcement.take() {
                        Some(announcement) => {
                            self.outbound = Some(TreeState::Advertise(
                                protocol::send_root_announcement(stream, announcement).boxed(),
                            ));
                        }
                        None => {
                            self.outbound = Some(TreeState::Idle(stream));
                        }
                    }
                    break;
                }
                Some(TreeState::OpenStream) => {
                    self.outbound = Some(TreeState::OpenStream);
                    break;
                }
                None => {
                    self.outbound = Some(TreeState::OpenStream);
                    let protocol = SubstreamProtocol::new(TreeProtocol, ());
                    return Poll::Ready(ProtocolsHandlerEvent::OutboundSubstreamRequest {
                        protocol,
                    });
                }
            }
        }

        Poll::Pending
    }
}

type SendFuture = BoxFuture<'static, Result<NegotiatedSubstream, io::Error>>;
type RecvFuture = BoxFuture<'static, Result<(NegotiatedSubstream, RootAnnouncement), io::Error>>;

/// The current state w.r.t. outbound announcements.
enum TreeState {
    /// A new substream is being negotiated for the tree protocol.
    OpenStream,
    /// The substream is idle, waiting to send the next announcement.
    Idle(NegotiatedSubstream),
    /// An announcement is being sent.
    Advertise(SendFuture),
}
