/// Defines the high level actions taken by the spanning tree.
pub mod behaviour;

/// Defines the handler for the spanning tree protocol.
pub mod handler;

/// Defines the spanning tree protocol.
pub mod protocol;
