/// Implementation of the Psion router backed by libp2p.
#[cfg(feature = "psion-libp2p")]
#[cfg_attr(docsrs, doc(cfg(feature = "psion-libp2p")))]
pub mod psion_libp2p;
