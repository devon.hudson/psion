use std::net::{SocketAddrV4, SocketAddrV6};

/// An address that can be used to extablish a direct network connection with a peer.
#[derive(Debug, Clone, PartialEq)]
pub enum PeerAddress {
    /// IPv4 socket and port.
    SocketV4(SocketAddrV4),
    /// IPv6 socket and port.
    SocketV6(SocketAddrV6),
    /// Generic raw string address representation.
    RawString(String),
}

pub type PeerAddresses = Vec<PeerAddress>;
