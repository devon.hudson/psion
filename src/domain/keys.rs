use log::info;
use rand::rngs::OsRng;

pub const PRIV_KEY_LENGTH: usize = ed25519_dalek::SECRET_KEY_LENGTH;
pub const PUB_KEY_LENGTH: usize = ed25519_dalek::PUBLIC_KEY_LENGTH;
pub const SIGNATURE_LENGTH: usize = ed25519_dalek::SIGNATURE_LENGTH;
pub type PublicKey = [u8; PUB_KEY_LENGTH];
pub type PrivateKey = [u8; PRIV_KEY_LENGTH];
pub type Signature = [u8; SIGNATURE_LENGTH];

/// ed25519 keypair
pub type KeyPair = (PrivateKey, PublicKey);

/// Generates an ed25519 keypair.
///
/// # Returns
///
/// A tuple containing two byte arrays.
/// The first byte array is the private key.
/// The second byte array is the public key.
pub fn generate_node_keypair() -> KeyPair {
    let mut csprng = OsRng {};
    let keypair = ed25519_dalek::Keypair::generate(&mut csprng);
    info!("Generated keypair: {:?}", keypair.to_bytes());
    (keypair.secret.to_bytes(), keypair.public.to_bytes())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn should_generate_new_keypair() {
        let _ = generate_node_keypair();
    }
}
