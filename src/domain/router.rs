use crate::domain::keys;
use crate::domain::peer::PeerAddresses;
use crate::domain::types::{PinnedFuture, SocketChannelPair};
use async_trait::async_trait;
use futures::channel::mpsc;
use std::{future::Future, marker::PhantomData, pin::Pin};
use thiserror::Error;

use super::keys::{PrivateKey, PublicKey};
use super::spanning_tree::node::Coordinates;

pub const MAX_SOCKET_BUFFER_SIZE: usize = 32;
// TODO : Should this be configurable?
pub const MAX_PEER_CONNECTIONS: usize = 10;

/// Events that are emitted from the router instance.
#[derive(Debug, Clone, PartialEq)]
pub enum RouterEvent {
    /// Emitted when a new peer connection is established.
    ConnectionEstablished,
    /// Emitted when a new spanning tree root is picked.
    NewSpanningTreeRoot {
        /// The public key of the new root node.
        root_key: PublicKey,
        /// The new coordinates of this node.
        coordinates: Coordinates,
        /// This node's parent's public key.
        parent: Option<PublicKey>,
    },
}

pub type EventSender = mpsc::Sender<RouterEvent>;

#[derive(Debug, Clone, PartialEq)]
pub enum RouterCommand {}

pub type CommandSender = mpsc::Sender<RouterCommand>;
pub type CommandReceiver = mpsc::Receiver<RouterCommand>;

/// Marker trait for an abstract `Future` executor.
/// Implemented for closures that spawn a task to run the provided `Future` in the background.
pub trait RouterExecutor:
    Fn(Pin<Box<dyn Future<Output = ()> + Send>>) + Send + 'static + Clone
{
    /// Run the given future in the background until it ends.
    fn exec(&self, future: PinnedFuture);
}

impl<F> RouterExecutor for F
where
    F: Fn(Pin<Box<dyn Future<Output = ()> + Send>>) + Send + 'static + Clone,
{
    fn exec(&self, f: PinnedFuture) {
        self(f)
    }
}

/// The Psion Router interface.
#[async_trait]
pub trait Router {
    /// Creates and starts a new instance of the router on the provided executor.
    fn new(
        key_pair: keys::KeyPair,
        executor: impl RouterExecutor,
        listen_addresses: PeerAddresses,
        static_peers: PeerAddresses,
        event_channel: Option<EventSender>,
    ) -> Result<Self, RouterError>
    where
        Self: Sized;

    /// Returns rx/tx channels to be used by an [OverlaySocket][`crate::domain::overlay_socket::OverlaySocket`] backed by the router instance.
    fn get_overlay_socket_channels(&mut self) -> Result<SocketChannelPair, RouterError>;

    /// Issues a command for the router instance to execute.
    fn send_command(&mut self, command: RouterCommand) -> Result<(), RouterError>;
}

/// A builder for [`Router`]s.
///
/// # Example
///
/// Static peers can be optionally provideded for the router to attempt direct connections to.
///
/// ```
/// # use psion::*;
/// # use async_std::task;
/// # use std::net::Ipv4Addr;
/// # use std::net::IpAddr;
/// # let (private_key, public_key) = generate_node_keypair();
/// let router =
///     RouterBuilder::<Libp2pRouter>::new(private_key, public_key)
///         .with_static_peers(vec![
///             PeerAddress::SocketV4("127.0.0.1:8080".parse().unwrap()),
///             PeerAddress::SocketV4("127.0.0.1:8080".parse().unwrap())
///         ])
///         .build(|fut| { task::spawn(fut); })
///         .unwrap();
/// ```
#[derive(Debug)]
pub struct RouterBuilder<T: Router> {
    key_pair: keys::KeyPair,
    router_type: PhantomData<T>,
    listen_addresses: PeerAddresses,
    static_peers: PeerAddresses,
    event_channel: Option<EventSender>,
}

impl<T: Router> RouterBuilder<T> {
    /// Creates a new [`RouterBuilder`].
    pub fn new(priv_key: PrivateKey, pub_key: PublicKey) -> Self {
        Self {
            key_pair: (priv_key, pub_key),
            router_type: PhantomData,
            listen_addresses: PeerAddresses::new(),
            static_peers: PeerAddresses::new(),
            event_channel: None,
        }
    }

    /// Configures the set of known static peer addresses.
    pub fn with_static_peers(mut self, peer_addresses: PeerAddresses) -> Self {
        self.static_peers = peer_addresses;
        self
    }

    /// Configures the set of listen addresses for this router to listen on.
    pub fn with_listen_addresses(mut self, listen_addresses: PeerAddresses) -> Self {
        self.listen_addresses = listen_addresses;
        self
    }

    /// Adds the sending side of a channel to the router for [`RouterEvent`] emission.
    pub fn with_event_channel(mut self, event_channel: EventSender) -> Self {
        self.event_channel = Some(event_channel);
        self
    }

    /// Uses the current [`RouterBuilder`] to generate a new [`Router`].
    pub fn build(&self, executor: impl RouterExecutor) -> Result<T, RouterError> {
        T::new(
            self.key_pair,
            executor,
            self.listen_addresses.clone(),
            self.static_peers.clone(),
            self.event_channel.clone(),
        )
    }
}

/// Error types returned from the `Router`.
#[derive(Error, Debug, PartialEq)]
pub enum RouterError {
    /// The provided node key is invalid.
    #[error("Invalid Key: `{0}`")]
    InvalidKey(String),
    /// The provided node keypair is invalid.
    #[error("Invalid Keypair")]
    InvalidKeyPair,
    /// New overlay socket channels were requested but they already exist.
    #[error("Overlay Socket Exists: `{0}`")]
    OverlaySocketExists(String),
    /// The `Router`s command channel is currently full.
    #[error("Command Channel Full")]
    CommandChannelFull,
    /// The `Router`s command channel has been disconnected.
    #[error("Command Channel Disconnected")]
    CommandChannelDisconnected,
}

impl From<ed25519_dalek::ed25519::Error> for RouterError {
    fn from(_: ed25519_dalek::ed25519::Error) -> Self {
        RouterError::InvalidKeyPair
    }
}
