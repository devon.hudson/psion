use crate::domain::types::{SocketReceiver, SocketSender};

/// A socket interface that can be backed by the psion `Router`.
#[derive(Debug)]
pub struct OverlaySocket {
    tx_channel: SocketSender,
    rx_channel: SocketReceiver,
}

impl OverlaySocket {
    /// Creates a new [`OverlaySocket`].
    pub fn new(tx: SocketSender, rx: SocketReceiver) -> Self {
        OverlaySocket {
            tx_channel: tx,
            rx_channel: rx,
        }
    }
}

#[cfg(test)]
mod tests {
    // [`OverlaySocket`] should pass this test in the future.
    // #[test]
    // fn socket_should_implement_minimum_traits() {
    //     fn assert_minimum_trait_bounds<T: AsyncRead + AsyncWrite + Unpin + Send>(_socket: T) {}

    //     let (priv_key, pub_key) = generate_node_keypair();
    //     let router = create_default_router(priv_key, pub_key);
    //     let socket = router.socket();
    //     assert_minimum_trait_bounds(socket);
    // }
}
