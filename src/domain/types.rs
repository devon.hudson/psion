use futures::channel::mpsc;
use std::{future::Future, pin::Pin, usize};

pub type SocketData = usize;

pub type SocketSender = mpsc::Sender<SocketData>;
pub type SocketReceiver = mpsc::Receiver<SocketData>;
pub type SocketChannelPair = (SocketSender, SocketReceiver);

pub type PinnedFuture = Pin<Box<dyn Future<Output = ()> + Send>>;

pub type Seconds = u64;
