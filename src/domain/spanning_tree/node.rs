use super::{
    announcement::{get_coordinates, RootAnnouncement},
    switch::{PeerState, Switch, ROOT_ANNOUNCEMENT_TIMEOUT},
};
use crate::domain::{keys::SIGNATURE_LENGTH, spanning_tree::announcement::SignatureWithHop};
use ed25519_dalek::Keypair;
use log::{debug, error, info, trace};
use std::{
    cmp::Ordering,
    fmt::Debug,
    time::{Duration, Instant, SystemTime, UNIX_EPOCH},
};

pub type Coordinate = u64;
pub type Coordinates = Vec<Coordinate>;

#[derive(Debug, PartialEq)]
pub enum AnnouncementResult {
    Dropped,
    RootUpdated,
    RootAndParentUpdated,
}

#[derive(Debug)]
pub struct Node<T>
where
    T: PartialEq + Debug + Copy,
{
    pub switch: Switch<T>,
    pub key_pair: Keypair,
    pub root_info: Option<RootAnnouncement>,
    pub parent_switch_port: Option<Coordinate>,
    pub coordinates: Coordinates,
}

impl<T> Node<T>
where
    T: PartialEq + Debug + Copy,
{
    pub fn new(key_pair: Keypair) -> Self {
        Self {
            switch: Switch::default(),
            key_pair,
            root_info: None,
            parent_switch_port: None,
            coordinates: Vec::new(),
        }
    }

    pub fn select_new_parent(&mut self) {
        // Start by setting self as root then try to find a better parent.
        self.root_info = None;
        self.parent_switch_port = None;
        self.coordinates = Vec::new();

        if let Some(coordinate) = find_new_parent(&self.switch) {
            if let Some(peer) = self.switch.get_peer_info(coordinate) {
                if let Some(announcement) = &peer.last_announcement {
                    if announcement.root_key > self.key_pair.public.to_bytes() {
                        self.root_info = Some(announcement.clone());
                        self.parent_switch_port = Some(coordinate);
                        self.coordinates = get_coordinates(&announcement.signatures)
                    }
                }
            }
        }
    }

    pub fn handle_announcement(
        &mut self,
        announcement: &RootAnnouncement,
        coordinate: Coordinate,
    ) -> AnnouncementResult {
        let mut result = AnnouncementResult::Dropped;

        for sig in announcement.signatures.iter() {
            if sig.public_key == self.key_pair.public.to_bytes() {
                // NOTE : If the signatures list already contains our public key it is a loopback packet so drop it.
                return result;
            }
        }

        match &self.root_info {
            Some(root_info) => {
                match announcement.root_key.cmp(&root_info.root_key) {
                    Ordering::Equal => {
                        match announcement
                            .signatures
                            .len()
                            .cmp(&(root_info.signatures.len() - 1)) // 1 less since it contains our sig
                        {
                            Ordering::Less => {
                                // NOTE : Don't change parent if the sequence number isn't higher.
                                if announcement.sequence > root_info.sequence {
                                    // A higher sequence number iplies a lower latency and shorter hop distance to the root.
                                    self.update_parent_and_root(announcement, coordinate);
                                    result = AnnouncementResult::RootAndParentUpdated;
                                }
                            }
                            Ordering::Equal => {
                                // NOTE : If the sequence number isn't higher drop this announcement.
                                if announcement.sequence > root_info.sequence {
                                    // NOTE : Only update root info in this case if it is from our parent.
                                    if let Some(port) = self.parent_switch_port {
                                        if port == coordinate {
                                            // NOTE : Add our signature to the announcement before caching it.
                                            self.root_info =
                                                Some(announcement.signed_copy(&self.key_pair));
                                            result = AnnouncementResult::RootUpdated;
                                        }
                                    }
                                }
                            }
                            Ordering::Greater => {} // Drop this announcement
                        }
                    }
                    Ordering::Greater => {
                        self.update_parent_and_root(announcement, coordinate);
                        result = AnnouncementResult::RootAndParentUpdated;
                    }
                    Ordering::Less => {} // Drop this announcement
                }
            }
            None => {
                if announcement.root_key > self.key_pair.public.to_bytes() {
                    self.update_parent_and_root(announcement, coordinate);
                    result = AnnouncementResult::RootAndParentUpdated;
                }
            }
        }

        result
    }

    fn update_parent_and_root(&mut self, announcement: &RootAnnouncement, port: Coordinate) {
        self.parent_switch_port = Some(port);
        // NOTE : Add our signature to the announcement before caching it.
        self.root_info = Some(announcement.signed_copy(&self.key_pair));
        self.coordinates = get_coordinates(&announcement.signatures);
    }

    pub fn generate_announcement(
        &self,
        peer: &PeerState<T>,
        switch_port: Coordinate,
    ) -> RootAnnouncement {
        info!("Announcement for: ({:}, {:?})", switch_port, peer);
        match &self.root_info {
            Some(root_info) => {
                let mut announcement = root_info.clone();
                // The last signature in the list should be ours at this point.
                if let Some(last_sig) = announcement.signatures.last_mut() {
                    // NOTE: public key and signature should already be correct
                    last_sig.hop = switch_port;
                }
                announcement
            }
            None => RootAnnouncement {
                root_key: self.key_pair.public.to_bytes(),
                sequence: SystemTime::now()
                    .duration_since(UNIX_EPOCH)
                    .unwrap()
                    .as_nanos() as u64,
                signatures: vec![SignatureWithHop {
                    hop: switch_port,
                    public_key: self.key_pair.public.to_bytes(),
                    // TODO : generate proper signature here.
                    signature: [0; SIGNATURE_LENGTH],
                }],
                timestamp: Instant::now(),
            },
        }
    }
}

fn find_new_parent<T>(peers: &Switch<T>) -> Option<Coordinate>
where
    T: PartialEq + Debug + Copy,
{
    if let Some(Some(new_parent)) = peers
        .iter()
        .filter(|peer| match peer.as_ref() {
            Some(peer) => has_valid_root_announcement(peer),
            _ => false,
        })
        // NOTE : The filter above should remove any peers without valid root announcements.
        // This means unwraps are safe here because of the filter.
        .reduce(|best, candidate| -> &Option<PeerState<T>> {
            let best_ann = best.as_ref().unwrap().last_announcement.as_ref().unwrap();
            let candidate_ann = candidate
                .as_ref()
                .unwrap()
                .last_announcement
                .as_ref()
                .unwrap();

            if is_better_parent_candidate(candidate_ann, best_ann) {
                trace!("Found better parent candidate: {:?}", candidate);
                candidate
            } else {
                best
            }
        })
    {
        // NOTE : We just found this peer in the switch so it's coordinate must exist.
        match peers.get_peer_coordinate(new_parent) {
            Ok(coordinate) => {
                debug!("Found best parent: {:?}", new_parent);
                return Some(coordinate);
            }
            Err(_) => {
                error!(
                    "Could not find switch coordinate for new parent... {:?}",
                    new_parent
                );
            }
        }
    }

    info!("No new parent candidate found");
    None
}

fn has_valid_root_announcement<T>(peer: &PeerState<T>) -> bool
where
    T: PartialEq + Debug + Copy,
{
    match &peer.last_announcement {
        Some(root_ann) => {
            root_ann.timestamp.elapsed() < Duration::from_secs(ROOT_ANNOUNCEMENT_TIMEOUT)
        }
        _ => false,
    }
}

fn is_better_parent_candidate(
    candidate_ann: &RootAnnouncement,
    current_ann: &RootAnnouncement,
) -> bool {
    candidate_ann.root_key > current_ann.root_key
        || (candidate_ann.root_key == current_ann.root_key
            && candidate_ann.sequence > current_ann.sequence)
        || (candidate_ann.root_key == current_ann.root_key
            && candidate_ann.sequence == current_ann.sequence
            && candidate_ann.signatures.len() < current_ann.signatures.len())
        || (candidate_ann.root_key == current_ann.root_key
            && candidate_ann.sequence == current_ann.sequence
            && candidate_ann.signatures.len() == current_ann.signatures.len()
            && candidate_ann.timestamp < current_ann.timestamp)
}

#[cfg(test)]
mod tests {
    use std::{
        error::Error,
        time::{Duration, Instant},
    };

    use super::{find_new_parent, Node};
    use crate::{
        domain::{
            keys::{PublicKey, SIGNATURE_LENGTH},
            spanning_tree::{
                announcement::{get_coordinates, RootAnnouncement, SignatureWithHop},
                node::AnnouncementResult,
                switch::{PeerState, ROOT_ANNOUNCEMENT_TIMEOUT},
            },
        },
        generate_node_keypair,
    };
    use ed25519_dalek::Keypair;

    #[allow(dead_code)]
    fn enable_logging() {
        std::env::set_var("RUST_LOG", "debug");
        env_logger::init();
    }

    fn get_test_peers() -> Vec<PeerState<PublicKey>> {
        let highest_key = PeerState {
            id: generate_node_keypair().1,
            last_announcement: Some(RootAnnouncement {
                root_key: [
                    0xff, 0xff, 0xff, 0xff, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1, 0x1, 0x1, 0x2, 0x2, 0x2,
                    0x2, 0x3, 0x3, 0x3, 0x3, 0x4, 0x4, 0x4, 0x4, 0x5, 0x5, 0x5, 0x5, 0x6, 0x6, 0x6,
                    0x6,
                ],
                sequence: 0,
                signatures: vec![
                    SignatureWithHop {
                        hop: 1,
                        public_key: PublicKey::default(),
                        signature: [0; SIGNATURE_LENGTH],
                    },
                    SignatureWithHop {
                        hop: 2,
                        public_key: PublicKey::default(),
                        signature: [0; SIGNATURE_LENGTH],
                    },
                    SignatureWithHop {
                        hop: 3,
                        public_key: PublicKey::default(),
                        signature: [0; SIGNATURE_LENGTH],
                    },
                ],
                timestamp: Instant::now(),
            }),
        };
        let lowest_key = PeerState {
            id: generate_node_keypair().1,
            last_announcement: Some(RootAnnouncement {
                root_key: [
                    0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1, 0x1, 0x1, 0x2, 0x2, 0x2, 0x2,
                    0x3, 0x3, 0x3, 0x3, 0x4, 0x4, 0x4, 0x4, 0x5, 0x5, 0x5, 0x5, 0x6, 0x6, 0x6, 0x6,
                ],
                sequence: 0,
                signatures: vec![
                    SignatureWithHop {
                        hop: 1,
                        public_key: PublicKey::default(),
                        signature: [0; SIGNATURE_LENGTH],
                    },
                    SignatureWithHop {
                        hop: 1,
                        public_key: PublicKey::default(),
                        signature: [0; SIGNATURE_LENGTH],
                    },
                ],
                timestamp: Instant::now(),
            }),
        };
        let no_announcement = PeerState {
            id: generate_node_keypair().1,
            last_announcement: None,
        };
        let highest_key_dup = PeerState {
            id: generate_node_keypair().1,
            last_announcement: Some(RootAnnouncement {
                root_key: [
                    0xff, 0xff, 0xff, 0xff, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1, 0x1, 0x1, 0x2, 0x2, 0x2,
                    0x2, 0x3, 0x3, 0x3, 0x3, 0x4, 0x4, 0x4, 0x4, 0x5, 0x5, 0x5, 0x5, 0x6, 0x6, 0x6,
                    0x6,
                ],
                sequence: 0,
                signatures: vec![SignatureWithHop {
                    hop: 1,
                    public_key: PublicKey::default(),
                    signature: [0; SIGNATURE_LENGTH],
                }],
                timestamp: Instant::now(),
            }),
        };
        let key_and_distance_dup = PeerState {
            id: generate_node_keypair().1,
            last_announcement: Some(RootAnnouncement {
                root_key: [
                    0xff, 0xff, 0xff, 0xff, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1, 0x1, 0x1, 0x2, 0x2, 0x2,
                    0x2, 0x3, 0x3, 0x3, 0x3, 0x4, 0x4, 0x4, 0x4, 0x5, 0x5, 0x5, 0x5, 0x6, 0x6, 0x6,
                    0x6,
                ],
                sequence: 0,
                signatures: vec![SignatureWithHop {
                    hop: 1,
                    public_key: PublicKey::default(),
                    signature: [0; SIGNATURE_LENGTH],
                }],
                timestamp: Instant::now() - Duration::from_secs(ROOT_ANNOUNCEMENT_TIMEOUT / 2),
            }),
        };
        let invalid_timestamp = PeerState {
            id: generate_node_keypair().1,
            last_announcement: Some(RootAnnouncement {
                root_key: [
                    0xff, 0xff, 0xff, 0xff, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1, 0x1, 0x1, 0x2, 0x2, 0x2,
                    0x2, 0x3, 0x3, 0x3, 0x3, 0x4, 0x4, 0x4, 0x4, 0x5, 0x5, 0x5, 0x5, 0x6, 0x6, 0x6,
                    0x6,
                ],
                sequence: 0,
                signatures: vec![SignatureWithHop {
                    hop: 1,
                    public_key: PublicKey::default(),
                    signature: [0; SIGNATURE_LENGTH],
                }],
                timestamp: Instant::now() - Duration::from_secs(ROOT_ANNOUNCEMENT_TIMEOUT * 2),
            }),
        };

        vec![
            highest_key,
            lowest_key,
            no_announcement,
            highest_key_dup,
            key_and_distance_dup,
            invalid_timestamp,
        ]
    }

    #[test]
    fn should_return_no_value_when_no_peers_are_connected() {
        let node = Node::<PublicKey>::new(Keypair::generate(&mut rand::rngs::OsRng {}));
        let new_parent = find_new_parent(&node.switch);
        assert!(new_parent.is_none());
    }

    #[test]
    fn should_not_select_peer_with_no_root_announcement_as_parent() {
        // Given: a tree node has only a peer with no last root announcement
        let test_peers = get_test_peers();
        let mut node = Node::<PublicKey>::new(Keypair::generate(&mut rand::rngs::OsRng {}));
        node.switch.add_peer(&test_peers[2]).unwrap();

        // When: a new parent is searched for
        let new_parent = find_new_parent(&node.switch);

        // Then: no valid parent is returned
        assert!(new_parent.is_none());
    }

    #[test]
    fn should_decide_parent_with_higher_root_key_as_better_parent() {
        // Given: A spanning tree node has a set of known peers
        let test_peers = get_test_peers();
        let mut node = Node::<PublicKey>::new(Keypair::generate(&mut rand::rngs::OsRng {}));
        node.switch.add_peer(&test_peers[0]).unwrap();
        node.switch.add_peer(&test_peers[1]).unwrap();

        // When: a new parent is searched for
        let new_parent = find_new_parent(&node.switch);

        // Then: the peer with the highest known root key is selected
        assert_eq!(
            node.switch
                .get_peer_info(new_parent.unwrap())
                .as_ref()
                .unwrap()
                .id,
            test_peers[0].id
        );
    }

    #[test]
    fn should_decide_parent_with_shorter_distance_as_better_parent() {
        // NOTE : All else being equal.
        // Given: a tree node has multiple peers with the same best root key
        let test_peers = get_test_peers();
        let mut node = Node::<PublicKey>::new(Keypair::generate(&mut rand::rngs::OsRng {}));
        node.switch.add_peer(&test_peers[0]).unwrap();
        node.switch.add_peer(&test_peers[3]).unwrap();

        // When: a new parent is searched for
        let new_parent = find_new_parent(&node.switch);

        // Then: the peer with the shortest hop distance is selected
        assert_eq!(
            node.switch
                .get_peer_info(new_parent.unwrap())
                .as_ref()
                .unwrap()
                .id,
            test_peers[3].id
        );
    }

    #[test]
    fn should_decide_parent_with_older_timestamp_as_better_parent() {
        // NOTE : All else being equal.
        // Given: a tree node has multiple peers with the same best root key and hop distance
        let test_peers = get_test_peers();
        let mut node = Node::<PublicKey>::new(Keypair::generate(&mut rand::rngs::OsRng {}));
        node.switch.add_peer(&test_peers[3]).unwrap();
        node.switch.add_peer(&test_peers[4]).unwrap();

        // When: a new parent is searched for
        let new_parent = find_new_parent(&node.switch);

        // Then: the peer with the oldest announcement timestamp is selected
        assert_eq!(
            node.switch
                .get_peer_info(new_parent.unwrap())
                .as_ref()
                .unwrap()
                .id,
            test_peers[4].id
        );
    }

    #[test]
    fn should_not_use_parent_with_stale_timestamp_when_deciding_best_parent() {
        // Given: a tree node has a peer with a timed out timestamp
        let test_peers = get_test_peers();
        let mut node = Node::<PublicKey>::new(Keypair::generate(&mut rand::rngs::OsRng {}));
        node.switch.add_peer(&test_peers[5]).unwrap();

        // When: a new parent is searched for
        let new_parent = find_new_parent(&node.switch);

        // Then: no valid parent is returned
        assert!(new_parent.is_none());
    }

    #[test]
    fn should_start_as_root_node() {
        // Given: A spanning tree node has just started
        let node = Node::<PublicKey>::new(Keypair::generate(&mut rand::rngs::OsRng {}));

        // Then: the node considers itself as the root node
        assert_eq!(node.coordinates.len(), 0);
        assert_eq!(node.parent_switch_port, None);
        assert_eq!(node.root_info, None);
    }

    #[test]
    fn should_update_root_information_when_assigning_new_parent() {
        // Given: A spanning tree node has a set of known peers
        let test_peers = get_test_peers();
        let mut node = Node::<PublicKey>::new(Keypair::generate(&mut rand::rngs::OsRng {}));
        node.switch.add_peer(&test_peers[0]).unwrap();

        // When: a new parent is selected
        node.select_new_parent();

        // Then: the root information stored by this node is updated
        assert_eq!(
            node.root_info.unwrap(),
            *test_peers[0].last_announcement.as_ref().unwrap()
        );
    }

    #[test]
    fn should_update_coordinates_when_assigning_new_parent() {
        // Given: A spanning tree node has a set of known peers
        let test_peers = get_test_peers();
        let mut node = Node::<PublicKey>::new(Keypair::generate(&mut rand::rngs::OsRng {}));
        node.switch.add_peer(&test_peers[0]).unwrap();

        // When: a new parent is selected
        node.select_new_parent();

        // Then: the coordinates of this node are updated
        assert_eq!(
            node.coordinates,
            get_coordinates(&test_peers[0].last_announcement.as_ref().unwrap().signatures)
        );
    }

    #[test]
    fn should_drop_announcement_with_longer_hop_distance() -> Result<(), Box<dyn Error>> {
        // Given: A spanning tree node has accepted a peer's root announcement
        let test_peers = get_test_peers();
        let key_pair = generate_node_keypair();
        let dalek_key_pair = Keypair::from_bytes([key_pair.0, key_pair.1].concat().as_slice())?;
        let dalek_key_pair_dup = Keypair::from_bytes([key_pair.0, key_pair.1].concat().as_slice())?;
        let mut node = Node::<PublicKey>::new(dalek_key_pair);
        node.switch.add_peer(&test_peers[0]).unwrap();
        node.switch.add_peer(&test_peers[3]).unwrap();

        // Ensure we start with a valid root announcement from a peer
        node.handle_announcement(test_peers[3].last_announcement.as_ref().unwrap(), 3);
        assert_eq!(
            node.root_info.clone().unwrap(),
            test_peers[3]
                .last_announcement
                .clone()
                .unwrap()
                .signed_copy(&dalek_key_pair_dup)
        );

        // When: an announcement is handled with a longer hop distance
        let result = node.handle_announcement(test_peers[0].last_announcement.as_ref().unwrap(), 0);

        // Then: the announcement is dropped
        assert_eq!(result, AnnouncementResult::Dropped);
        Ok(())
    }

    #[test]
    fn should_drop_announcement_with_lesser_root_key() -> Result<(), Box<dyn Error>> {
        // Given: A spanning tree node has accepted a peer's root announcement
        let test_peers = get_test_peers();
        let key_pair = generate_node_keypair();
        let dalek_key_pair = Keypair::from_bytes([key_pair.0, key_pair.1].concat().as_slice())?;
        let dalek_key_pair_dup = Keypair::from_bytes([key_pair.0, key_pair.1].concat().as_slice())?;
        let mut node = Node::<PublicKey>::new(dalek_key_pair);
        node.switch.add_peer(&test_peers[0]).unwrap();
        node.switch.add_peer(&test_peers[1]).unwrap();

        // Ensure we start with a valid root announcement from a peer
        node.handle_announcement(test_peers[0].last_announcement.as_ref().unwrap(), 0);
        assert_eq!(
            node.root_info.clone().unwrap(),
            test_peers[0]
                .last_announcement
                .clone()
                .unwrap()
                .signed_copy(&dalek_key_pair_dup)
        );

        // When: an announcement is handled with a lesser root key
        let result = node.handle_announcement(test_peers[1].last_announcement.as_ref().unwrap(), 1);

        // Then: the announcement is dropped
        assert_eq!(result, AnnouncementResult::Dropped);
        Ok(())
    }

    #[test]
    fn should_drop_announcement_with_same_root_info_not_from_parent() -> Result<(), Box<dyn Error>>
    {
        // Given: A spanning tree node has accepted a peer's root announcement
        let test_peers = get_test_peers();
        let key_pair = generate_node_keypair();
        let dalek_key_pair = Keypair::from_bytes([key_pair.0, key_pair.1].concat().as_slice())?;
        let dalek_key_pair_dup = Keypair::from_bytes([key_pair.0, key_pair.1].concat().as_slice())?;
        let mut node = Node::<PublicKey>::new(dalek_key_pair);
        node.switch.add_peer(&test_peers[3]).unwrap();
        node.switch.add_peer(&test_peers[4]).unwrap();

        // Ensure we start with a valid root announcement from a peer
        node.handle_announcement(test_peers[3].last_announcement.as_ref().unwrap(), 3);
        assert_eq!(
            node.root_info.clone().unwrap(),
            test_peers[3]
                .last_announcement
                .clone()
                .unwrap()
                .signed_copy(&dalek_key_pair_dup)
        );

        // When: an announcement is handled with a matching root key and hop distance
        let result = node.handle_announcement(test_peers[4].last_announcement.as_ref().unwrap(), 4);

        // Then: the announcement is dropped
        assert_eq!(result, AnnouncementResult::Dropped);
        Ok(())
    }
}
