use super::announcement::RootAnnouncement;
use super::node::Coordinate;
use crate::domain::{router::MAX_PEER_CONNECTIONS, types::Seconds};
use core::fmt::Debug;
use log::{info, warn};
use std::ops::Deref;

pub const ROOT_ANNOUNCEMENT_INTERVAL: Seconds = 15 * 60; // 15 minutes
pub const ROOT_ANNOUNCEMENT_TIMEOUT: Seconds = ROOT_ANNOUNCEMENT_INTERVAL * 2;

/// Errors that can come from spanning tree actions.
#[derive(thiserror::Error, Debug, PartialEq)]
pub enum SwitchError {
    /// There is no space available for new peer connections.
    #[error("No available connections")]
    NoAvailableConnections,
    /// The peer already is connected to the spanning tree.
    #[error("Peer already connected")]
    PeerAlreadyConnected,
    /// The peer is not connected to the spanning tree.
    #[error("Peer not connected")]
    PeerNotConnected,
}

#[derive(Debug, Clone)]
pub struct PeerState<T>
where
    T: PartialEq + Debug + Copy,
{
    pub id: T,
    pub last_announcement: Option<RootAnnouncement>,
}

#[derive(Debug)]
pub struct Switch<T>(Vec<Option<PeerState<T>>>)
where
    T: PartialEq + Debug + Copy;

impl<T> Switch<T>
where
    T: PartialEq + Debug + Copy,
{
    pub fn add_peer(&mut self, peer: &PeerState<T>) -> Result<Coordinate, SwitchError> {
        if let Some(coordinate) = self.0.iter().position(|i| match i {
            Some(node) => node.id == peer.id,
            None => false,
        }) {
            warn!(
                "Peer is already connected at coord {}: {:?}",
                coordinate, peer
            );
            return Err(SwitchError::PeerAlreadyConnected);
        }

        match self.0.iter().position(|i| i.is_none()) {
            Some(coordinate) => {
                info!("Adding peer at coord {}: {:?}", coordinate, peer);
                self.0[coordinate] = Some(peer.clone());
                Ok(coordinate as u64)
            }
            None => {
                warn!("No available space. Failed adding peer: {:?}", peer);
                Err(SwitchError::NoAvailableConnections)
            }
        }
    }

    pub fn remove_peer(&mut self, peer: &PeerState<T>) {
        match self.0.iter().position(|i| match i {
            Some(node) => node.id == peer.id,
            None => false,
        }) {
            Some(coordinate) => {
                self.0[coordinate] = None;
            }
            None => warn!("Removing unconnected peer: {:?}", peer),
        }
    }

    pub fn get_peer_info(&self, coordinate: Coordinate) -> &Option<PeerState<T>> {
        &self.0[coordinate as usize]
    }

    pub fn get_peer_coordinate(&self, peer: &PeerState<T>) -> Result<Coordinate, SwitchError> {
        match self.0.iter().position(|i| match i {
            Some(node) => node.id == peer.id,
            None => false,
        }) {
            Some(coord) => Ok(coord as u64),
            None => {
                warn!("Peer wasn't found in the switch: {:?}", peer);
                Err(SwitchError::PeerNotConnected)
            }
        }
    }
}

impl<T> Default for Switch<T>
where
    T: PartialEq + Debug + Copy,
{
    fn default() -> Self {
        Switch(vec![None; MAX_PEER_CONNECTIONS])
    }
}

impl<T> Deref for Switch<T>
where
    T: PartialEq + Debug + Copy,
{
    type Target = Vec<Option<PeerState<T>>>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

mod tests {
    #[allow(unused_imports)]
    use super::*;

    #[test]
    fn should_add_peer_to_switch() {
        // Given: there is a peer to add to the spanning tree switch
        let peer = PeerState {
            id: 7,
            last_announcement: None,
        };
        let mut switch = Switch::<usize>::default();

        // When: the peer is added to the switch
        switch.add_peer(&peer).unwrap();

        // Then: the peer can be found in the switch
        assert!(switch.get_peer_coordinate(&peer).is_ok());
    }

    #[test]
    fn should_fail_to_add_same_peer_to_switch_twice() {
        // Given: there is a peer to add to the spanning tree switch
        let peer = PeerState {
            id: 7,
            last_announcement: None,
        };
        let mut switch = Switch::<usize>::default();

        // When: the peer is added to the switch
        switch.add_peer(&peer).unwrap();

        // Then: the same peer cannot be added to the switch again
        assert!(switch.add_peer(&peer).is_err());
    }

    #[test]
    fn should_remove_existing_peer_from_switch() {
        // Given: there is a peer added to the spanning tree switch
        let peer = PeerState {
            id: 7,
            last_announcement: None,
        };
        let mut switch = Switch::<usize>::default();
        switch.add_peer(&peer).unwrap();

        // When: the peer is removed from the switch
        switch.remove_peer(&peer);

        // Then: the peer can no longer be found in the switch
        assert!(switch.get_peer_coordinate(&peer).is_err());
    }

    #[test]
    fn should_silently_remove_non_existent_peer_from_switch() {
        // Given: there is a spanning tree switch
        let switch = Switch::<usize>::default();

        // When: no peer is added to the switch
        let peer = PeerState {
            id: 7,
            last_announcement: None,
        };

        // Then: the peer can not be found in the switch
        assert!(switch.get_peer_coordinate(&peer).is_err());
    }
}
