use super::node::{Coordinate, Coordinates};
use crate::domain::{
    frame::{Frame, FrameType, SIGNATURE_SIZE},
    keys::{PublicKey, Signature},
};
use bytes::{Buf, BufMut, Bytes};
use ed25519_dalek::Keypair;
use std::{
    convert::TryInto,
    io::{self},
    time::Instant,
};

pub fn get_coordinates(signatures: &[SignatureWithHop]) -> Coordinates {
    let mut coordinates = Vec::<Coordinate>::new();

    signatures.iter().for_each(|sig| coordinates.push(sig.hop));

    coordinates
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct SignatureWithHop {
    pub hop: u64,
    pub public_key: PublicKey,
    pub signature: Signature,
}

#[derive(Debug, Clone, PartialEq)]
pub struct RootAnnouncement {
    pub root_key: PublicKey,
    pub sequence: u64,
    pub signatures: Vec<SignatureWithHop>,
    pub timestamp: Instant,
}

impl RootAnnouncement {
    pub fn serialize(&self) -> Bytes {
        let mut payload = Frame::new(
            FrameType::Stp,
            (self.root_key.len() + 8 /* Sequence */ + (self.signatures.len() * (SIGNATURE_SIZE)))
                .try_into()
                .unwrap(),
        )
        .serialize()
        .to_vec();

        payload.put_slice(&self.root_key);
        payload.put_u64(self.sequence);
        self.signatures.iter().for_each(|sig| {
            payload.put_u64(sig.hop);
            payload.put_slice(&sig.public_key);
            payload.put_slice(&sig.signature);
        });

        Bytes::from(payload)
    }

    pub fn deserialize(payload: &mut Bytes) -> io::Result<Self> {
        let mut root_key = PublicKey::default();
        payload.copy_to_slice(&mut root_key);

        let sequence = payload.get_u64();

        if payload.remaining() % SIGNATURE_SIZE != 0 {
            return Err(io::Error::new(
                io::ErrorKind::InvalidData,
                "The list of signatures is corrupted",
            ));
        }
        let num_signatures = payload.remaining() / SIGNATURE_SIZE;
        let mut signatures = Vec::new();

        for _ in 1..=num_signatures {
            let hop = payload.get_u64();

            let mut public_key = [0u8; 32];
            payload.copy_to_slice(&mut public_key);

            let mut signature = [0u8; 64];
            payload.copy_to_slice(&mut signature);

            signatures.push(SignatureWithHop {
                hop,
                public_key,
                signature,
            });
        }

        Ok(RootAnnouncement {
            root_key,
            sequence,
            signatures,
            timestamp: Instant::now(),
        })
    }

    /// Generate a copy of the current anouncement with our signature added.
    pub fn signed_copy(&self, key_pair: &Keypair) -> RootAnnouncement {
        let mut new_announcement = self.clone();

        new_announcement.signatures.push(SignatureWithHop {
            // NOTE : The hop field is a placeholder until announcements are sent out.
            hop: 0,
            public_key: key_pair.public.to_bytes(),
            signature: [0u8; 64],
        });

        new_announcement
    }
}

#[cfg(test)]
mod tests {
    use std::time::{SystemTime, UNIX_EPOCH};

    use crate::domain::keys::SIGNATURE_LENGTH;

    #[allow(unused_imports)]
    use super::*;

    fn generate_announcement() -> RootAnnouncement {
        RootAnnouncement {
            root_key: [
                0xff, 0xff, 0xff, 0xff, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1, 0x1, 0x1, 0x2, 0x2, 0x2, 0x2,
                0x3, 0x3, 0x3, 0x3, 0x4, 0x4, 0x4, 0x4, 0x5, 0x5, 0x5, 0x5, 0x6, 0x6, 0x6, 0x6,
            ],
            sequence: SystemTime::now()
                .duration_since(UNIX_EPOCH)
                .unwrap()
                .as_nanos() as u64,
            signatures: vec![SignatureWithHop {
                hop: 1,
                public_key: [
                    0xff, 0xff, 0xff, 0xff, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1, 0x1, 0x1, 0x2, 0x2, 0x2,
                    0x2, 0x3, 0x3, 0x3, 0x3, 0x4, 0x4, 0x4, 0x4, 0x5, 0x5, 0x5, 0x5, 0x6, 0x6, 0x6,
                    0x6,
                ],
                // TODO : generate proper signature here.
                signature: [0; SIGNATURE_LENGTH],
            }],
            timestamp: Instant::now(),
        }
    }

    #[test]
    fn should_convert_to_and_from_serialized_format() {
        let announcement = generate_announcement();
        let mut serialized = announcement.serialize();

        // First strip off the frame header.
        let _frame = Frame::deserialize(&mut serialized);
        let deserialized = RootAnnouncement::deserialize(&mut serialized).unwrap();

        assert_eq!(deserialized.root_key, announcement.root_key);
        assert_eq!(deserialized.sequence, announcement.sequence);
        assert_eq!(deserialized.signatures, announcement.signatures);
    }
}
