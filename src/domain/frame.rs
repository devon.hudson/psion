use bytes::{Buf, BufMut, Bytes};
use std::{
    convert::{TryFrom, TryInto},
    io,
};

#[repr(u8)]
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum FrameType {
    Stp = 0,
}

impl TryFrom<u8> for FrameType {
    type Error = String;

    fn try_from(v: u8) -> Result<Self, Self::Error> {
        match v {
            x if x == FrameType::Stp as u8 => Ok(FrameType::Stp),
            x => Err(format!("Cannot convert {} into FrameType", x)),
        }
    }
}

pub const MAX_FRAME_SIZE: usize = 65535;
pub const NUM_MAGIC_BYTES: usize = 4;
pub const FRAME_VERSION: u8 = 0;
pub const FRAME_MAGIC_BYTES: [u8; NUM_MAGIC_BYTES] = [0x70, 0x69, 0x6e, 0x65];
pub const FRAME_HEADER_SIZE: usize = 8; // 4 magic bytes, version byte, type byte, data length 2 bytes
pub const SIGNATURE_SIZE: usize = 8 /* hop */ + 32 /* PublicKey */ + 64 /* Signature */;

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Frame {
    version: u8,
    frame_type: FrameType,
    length: u16,
}

impl Frame {
    pub fn new(frame_type: FrameType, length: u16) -> Self {
        Self {
            version: FRAME_VERSION,
            frame_type,
            length,
        }
    }

    pub fn len(&self) -> u16 {
        self.length
    }

    pub fn serialize(&self) -> Bytes {
        let mut payload = vec![];
        payload.put_slice(&FRAME_MAGIC_BYTES);
        payload.put_u8(self.version);
        payload.put_u8(self.frame_type as u8);
        payload.put_u16(self.length);

        Bytes::from(payload)
    }

    pub fn deserialize(payload: &mut Bytes) -> io::Result<Self> {
        let mut magic_bytes = [0u8; NUM_MAGIC_BYTES];
        payload.copy_to_slice(&mut magic_bytes);

        if magic_bytes != FRAME_MAGIC_BYTES {
            return Err(io::Error::new(
                io::ErrorKind::InvalidData,
                "The frame's magic bytes are wrong!",
            ));
        }

        let version = payload.get_u8();
        let try_type: Result<FrameType, _> = payload.get_u8().try_into();
        let length = payload.get_u16();
        if length as usize + FRAME_HEADER_SIZE > MAX_FRAME_SIZE {
            return Err(io::Error::new(
                io::ErrorKind::InvalidData,
                "The received payload length exceeds the maximum size of a frame",
            ));
        }

        match try_type {
            Ok(frame_type) => Ok(Self {
                version,
                frame_type,
                length,
            }),
            Err(e) => Err(io::Error::new(
                io::ErrorKind::InvalidData,
                format!("Frame type not recognized: {:?}", e),
            )),
        }
    }
}

#[cfg(test)]
mod tests {
    #[allow(unused_imports)]
    use super::*;

    #[test]
    fn should_convert_to_and_from_serialized_format() {
        let frame = Frame::new(FrameType::Stp, 10);
        let mut serialized = frame.serialize();
        let deserialized = Frame::deserialize(&mut serialized).unwrap();

        assert_eq!(frame, deserialized);
    }
}
