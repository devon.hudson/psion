#![deny(rust_2018_idioms, missing_docs, missing_debug_implementations)]
#![forbid(unsafe_code)]
#![cfg_attr(docsrs, feature(doc_cfg))]
//! # Psion - API Documentation
//!
//! Welcome to the Psion API documentation!
//!
//! [psion-libp2p]: crate::adapters::psion_libp2p
//! [adapters]: crate::adapters
//!
//! ## Usage
//!
//! First generate a new keypair for the node if you don't already have one:
//!
//! ```
//! # use psion::*;
//! let (private_key, public_key) = generate_node_keypair();
//! ```
//!
//! Next use the [`RouterBuilder`] to create a new [`Router`] instance.
//!
//! ```
//! # use psion::*;
//! # use async_std::task;
//! # let (private_key, public_key) = generate_node_keypair();
//! let router =
//!     RouterBuilder::<Libp2pRouter>::new(private_key, public_key)
//!         .build(|fut| { task::spawn(fut); }).unwrap();
//! ```
//!
//! ## Feature Flags
//!
//! Psion uses a set of [feature flags] to reduce the amount of compiled code.
//!
//! - `psion-libp2p`: Enables the [psion-libp2p] based implementation of [`Router`].
//!
//! [feature flags]: https://doc.rust-lang.org/cargo/reference/manifest.html#the-features-section

mod adapters;
#[cfg(feature = "psion-libp2p")]
pub use crate::adapters::psion_libp2p;
pub use crate::adapters::psion_libp2p::Libp2pRouter;

mod domain;
pub use crate::domain::{
    keys::generate_node_keypair,
    overlay_socket::OverlaySocket,
    peer::PeerAddress,
    router::{Router, RouterBuilder, RouterError, RouterEvent, RouterExecutor},
};
