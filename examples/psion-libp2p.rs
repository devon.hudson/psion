use async_std::task;
use clap::{App, Arg};
use futures_timer::Delay;
use psion::{
    generate_node_keypair, Libp2pRouter, OverlaySocket, PeerAddress, Router, RouterBuilder,
};
use std::{error::Error, time::Duration};

#[async_std::main]
async fn main() -> Result<(), Box<dyn Error>> {
    std::env::set_var("RUST_LOG", "info");
    env_logger::init();

    let matches = App::new("psion-libp2p")
        .about("Starts a new psion router instance using the libp2p backend.")
        .arg(
            Arg::with_name("LISTEN")
                .help("The TCP address to listen on for peer connections.")
                .short("l")
                .default_value("127.0.0.1:0")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("DIAL")
                .help("The TCP address to dial to make a peer connection.")
                .short("d")
                .default_value("127.0.0.1:8080")
                .takes_value(true),
        )
        .get_matches();

    let listen = matches.value_of("LISTEN").unwrap();
    let dial = matches.value_of("DIAL").unwrap();

    let (priv_key, pub_key) = generate_node_keypair();
    let mut router = RouterBuilder::<Libp2pRouter>::new(priv_key, pub_key)
        .with_listen_addresses(vec![PeerAddress::SocketV4(listen.parse()?)])
        .with_static_peers(vec![PeerAddress::SocketV4(dial.parse()?)])
        .build(|fut| {
            task::spawn(fut);
        })
        .unwrap();

    let (tx_channel, rx_channel) = router.get_overlay_socket_channels()?;
    let _socket = OverlaySocket::new(tx_channel, rx_channel);

    async {
        loop {
            Delay::new(Duration::from_secs(10)).await
        }
    }
    .await;

    Ok(())
}
